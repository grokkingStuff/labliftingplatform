EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 17400 6950 0    50   ~ 0
Inter-Integrated Circuit (I2C)\n\nSDA: Serial Data, used to send and receive data\nSCL: Serial Clock, used to carry clock signal\n\nSynchronous protocol with only two wires. \nUsed quite a bit to interface with sensors.\n\nUnfortunately, two conductive wires that extend for some \ndistance acts as a "bus capacitor" that  degrades signal \nquality. Using stronger pullup resistors can charge that \nparasitic capacitor quicker but also means the higher current \nthat flows through your fragile sensors, which places a hard \nlimit on such finesses. Using a differential pair helps reduce \nthe parastic losses and is more able to handle EMI.
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415043
P 10300 2250
F 0 "#GND?" H 10350 2200 45  0001 L BNN
F 1 "GND" H 10300 2080 45  0000 C CNN
F 2 "" H 10300 2150 60  0001 C CNN
F 3 "" H 10300 2150 60  0001 C CNN
	1    10300 2250
	1    0    0    -1  
$EndComp
Text GLabel 12350 1900 0    50   Input ~ 0
DSCL0_N
Text GLabel 12350 1500 0    50   Input ~ 0
DSCL0_P
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415047
P 12500 2300
F 0 "#GND?" H 12550 2250 45  0001 L BNN
F 1 "GND" H 12500 2130 45  0000 C CNN
F 2 "" H 12500 2200 60  0001 C CNN
F 3 "" H 12500 2200 60  0001 C CNN
	1    12500 2300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E99279F
P 12500 1300
F 0 "R?" V 12405 1368 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 1368 45  0000 L CNN
F 2 "0603" H 12500 1450 20  0001 C CNN
F 3 "" H 12500 1300 60  0001 C CNN
F 4 "RES-07857" V 12584 1368 60  0000 L CNN "Field4"
	1    12500 1300
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E9927A6
P 12500 1700
F 0 "R?" V 12405 1768 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 1768 45  0000 L CNN
F 2 "0603" H 12500 1850 20  0001 C CNN
F 3 "" H 12500 1700 60  0001 C CNN
F 4 "RES-07857" V 12584 1768 60  0000 L CNN "Field4"
	1    12500 1700
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E9927AD
P 12500 2100
F 0 "R?" V 12405 2168 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 2168 45  0000 L CNN
F 2 "0603" H 12500 2250 20  0001 C CNN
F 3 "" H 12500 2100 60  0001 C CNN
F 4 "RES-07857" V 12584 2168 60  0000 L CNN "Field4"
	1    12500 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	12350 1500 12500 1500
Connection ~ 12500 1500
Wire Wire Line
	12350 1900 12500 1900
Connection ~ 12500 1900
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E9959EE
P 12500 1100
F 0 "#SUPPLY?" H 12550 1100 45  0001 L BNN
F 1 "3.3V" H 12500 1270 45  0000 C CNN
F 2 "" H 12500 1281 60  0000 C CNN
F 3 "" H 12500 1100 60  0001 C CNN
	1    12500 1100
	1    0    0    -1  
$EndComp
Text GLabel 12650 1500 2    50   Output ~ 0
DSCLP
Text GLabel 12650 1900 2    50   Output ~ 0
DSCLM
Wire Wire Line
	12650 1500 12500 1500
Wire Wire Line
	12650 1900 12500 1900
Text GLabel 16900 7400 2    50   Input ~ 0
DSDAP
Text GLabel 16900 7500 2    50   Input ~ 0
DSDAM
Text GLabel 16900 7300 2    50   Input ~ 0
DSCLP
Text GLabel 16900 7200 2    50   Input ~ 0
DSCLM
Text GLabel 16900 7600 2    50   Output ~ 0
SCL
Text GLabel 16900 7700 2    50   Output ~ 0
SDA
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5F41504D
P 15800 9350
F 0 "C?" V 16140 9400 45  0000 C CNN
F 1 "0.1UF-0603-100V-10%" V 16056 9400 45  0000 C CNN
F 2 "0603" H 15800 9600 20  0001 C CNN
F 3 "" H 15800 9350 50  0001 C CNN
F 4 "CAP-08390" V 15961 9400 60  0000 C CNN "Field4"
	1    15800 9350
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-Capacitors:100UF-POLAR-10X10.5-63V-20% C?
U 1 1 5E9A70A6
P 15800 8850
F 0 "C?" V 15440 8800 45  0000 C CNN
F 1 "100UF-POLAR-10X10.5-63V-20%" V 15524 8800 45  0000 C CNN
F 2 "NIC_10X10.5_CAP" H 15800 9100 20  0001 C CNN
F 3 "" H 15800 8850 50  0001 C CNN
F 4 "CAP-08362" V 15619 8800 60  0000 C CNN "Field4"
	1    15800 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	10450 1850 10300 1850
Wire Wire Line
	10450 1450 10300 1450
Text GLabel 10450 1450 2    50   Output ~ 0
DSDAP
Text GLabel 10450 1850 2    50   Output ~ 0
DSDAM
Connection ~ 10300 1850
Wire Wire Line
	10150 1850 10300 1850
Connection ~ 10300 1450
Wire Wire Line
	10150 1450 10300 1450
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98D8A7
P 10300 2050
F 0 "R?" V 10205 2118 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 2118 45  0000 L CNN
F 2 "0603" H 10300 2200 20  0001 C CNN
F 3 "" H 10300 2050 60  0001 C CNN
F 4 "RES-07857" V 10384 2118 60  0000 L CNN "Field4"
	1    10300 2050
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98D37A
P 10300 1650
F 0 "R?" V 10205 1718 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 1718 45  0000 L CNN
F 2 "0603" H 10300 1800 20  0001 C CNN
F 3 "" H 10300 1650 60  0001 C CNN
F 4 "RES-07857" V 10384 1718 60  0000 L CNN "Field4"
	1    10300 1650
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98C2CF
P 10300 1250
F 0 "R?" V 10205 1318 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 1318 45  0000 L CNN
F 2 "0603" H 10300 1400 20  0001 C CNN
F 3 "" H 10300 1250 60  0001 C CNN
F 4 "RES-07857" V 10384 1318 60  0000 L CNN "Field4"
	1    10300 1250
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E98A2EF
P 10300 1050
F 0 "#SUPPLY?" H 10350 1050 45  0001 L BNN
F 1 "3.3V" H 10300 1220 45  0000 C CNN
F 2 "" H 10300 1231 60  0000 C CNN
F 3 "" H 10300 1050 60  0001 C CNN
	1    10300 1050
	1    0    0    -1  
$EndComp
Text GLabel 10150 1450 0    50   Input ~ 0
DSDA0_P
Text GLabel 10150 1850 0    50   Input ~ 0
DSDA0_N
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C9695
P 2350 8900
F 0 "JP?" H 2454 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 9150 20  0001 C CNN
F 3 "" H 2350 8900 60  0001 C CNN
F 4 "" H 2454 8816 60  0000 L CNN "PROD_ID"
	1    2350 8900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_1-NC JP?
U 1 1 5F415056
P 1850 8900
F 0 "JP?" H 1954 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_1-NC" H 1954 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_1-NC" H 1850 9150 20  0001 C CNN
F 3 "" V 1850 8845 60  0001 C CNN
F 4 "" H 1954 8984 60  0000 L CNN "PROD_ID"
	1    1850 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 8050 1850 8200
Wire Wire Line
	2350 8200 2350 8050
Wire Wire Line
	2350 7650 2350 7550
Wire Wire Line
	2350 7550 1850 7550
Wire Wire Line
	1850 7550 1850 7650
Wire Wire Line
	2350 8700 2350 8600
Wire Wire Line
	2350 8600 1850 8600
Wire Wire Line
	1850 8600 1850 8700
Wire Wire Line
	1850 9100 1850 9250
Wire Wire Line
	1850 9250 2350 9250
Wire Wire Line
	2350 9250 2350 9100
$Comp
L dk_PMIC-Current-Regulation-Management:INA219AIDCNR U?
U 1 1 5E9EFC77
P 4050 8100
F 0 "U?" H 4178 8103 60  0000 L CNN
F 1 "INA219AIDCNR" H 4178 7997 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-8" H 4250 8300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8400 60  0001 L CNN
F 4 "296-23770-1-ND" H 4250 8500 60  0001 L CNN "Digi-Key_PN"
F 5 "INA219AIDCNR" H 4250 8600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4250 8700 60  0001 L CNN "Category"
F 7 "PMIC - Current Regulation/Management" H 4250 8800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/INA219AIDCNR/296-23770-1-ND/1952550" H 4250 9000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC CURRENT MONITOR 1% SOT23-8" H 4250 9100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4250 9200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4250 9300 60  0001 L CNN "Status"
	1    4050 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 8200 1850 8200
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C969C
P 1850 7850
F 0 "JP?" H 1954 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1954 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 1850 8100 20  0001 C CNN
F 3 "" H 1850 7850 60  0001 C CNN
F 4 "" H 1954 7766 60  0000 L CNN "PROD_ID"
	1    1850 7850
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C578F
P 2350 7850
F 0 "JP?" H 2454 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 8100 20  0001 C CNN
F 3 "" H 2350 7850 60  0001 C CNN
F 4 "" H 2454 7766 60  0000 L CNN "PROD_ID"
	1    2350 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 8900 2200 8900
Wire Wire Line
	2000 8900 2100 8900
Connection ~ 2100 8900
Wire Wire Line
	2100 8400 2100 8900
Wire Wire Line
	2000 7850 2100 7850
Wire Wire Line
	2100 8300 2100 7850
Connection ~ 2100 7850
Wire Wire Line
	2100 7850 2200 7850
Wire Wire Line
	2350 7550 3150 7550
Wire Wire Line
	3150 7550 3150 8100
Connection ~ 2350 7550
Connection ~ 2350 8200
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F41505C
P 950 7100
F 0 "#SUPPLY?" H 1000 7100 45  0001 L BNN
F 1 "VCC" H 950 7270 45  0000 C CNN
F 2 "" H 950 7281 60  0000 C CNN
F 3 "" H 950 7100 60  0001 C CNN
	1    950  7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  8600 1850 8600
Connection ~ 1850 8600
Wire Wire Line
	4050 7200 4050 7700
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5EA31DD3
P 3750 9000
F 0 "C?" H 3858 9145 45  0000 L CNN
F 1 "0.1UF-0603-100V-10%" H 3858 9061 45  0000 L CNN
F 2 "0603" H 3750 9250 20  0001 C CNN
F 3 "" H 3750 9000 50  0001 C CNN
F 4 "CAP-08390" H 3858 8966 60  0000 L CNN "Field4"
	1    3750 9000
	-1   0    0    1   
$EndComp
Connection ~ 2350 8600
Wire Wire Line
	3750 9200 3750 9250
Wire Wire Line
	3750 9250 4050 9250
Wire Wire Line
	4050 9250 4050 8600
Connection ~ 2350 9250
Connection ~ 3750 9250
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA41E4C
P 950 9350
F 0 "#GND?" H 1000 9300 45  0001 L BNN
F 1 "GND" H 950 9180 45  0000 C CNN
F 2 "" H 950 9250 60  0001 C CNN
F 3 "" H 950 9250 60  0001 C CNN
	1    950  9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 9250 950  9250
Wire Wire Line
	950  9250 950  9350
Connection ~ 1850 9250
Text Notes 1100 8400 0    50   ~ 0
I2C Address Selection
Wire Wire Line
	3650 7900 3650 7600
Wire Wire Line
	3650 8000 3550 8000
Wire Wire Line
	3550 8000 3550 7500
Wire Wire Line
	3650 7600 4500 7600
Wire Wire Line
	3550 7500 4500 7500
Text GLabel 4500 7600 2    50   Output ~ 0
IN+
Text GLabel 4500 7500 2    50   Output ~ 0
IN-
Wire Wire Line
	3650 8200 3450 8200
Text GLabel 4500 7400 2    50   BiDi ~ 0
SDA
Text GLabel 4500 7300 2    50   BiDi ~ 0
SCL
Wire Wire Line
	4500 7400 3450 7400
Wire Wire Line
	3450 7400 3450 8200
Connection ~ 3450 8200
Wire Wire Line
	4500 7300 3350 7300
Wire Wire Line
	3350 7300 3350 8100
Connection ~ 3350 8100
Wire Wire Line
	3350 8100 3650 8100
Wire Wire Line
	2100 8300 3650 8300
Wire Wire Line
	2100 8400 3650 8400
Wire Wire Line
	3150 8100 3350 8100
Wire Wire Line
	2350 8200 3450 8200
Wire Wire Line
	2350 8600 3750 8600
Wire Wire Line
	2350 9250 3750 9250
Wire Wire Line
	15500 7850 15400 7850
Wire Wire Line
	15400 7850 15400 7500
Wire Wire Line
	15500 7950 15300 7950
Wire Wire Line
	15300 7950 15300 7400
Wire Wire Line
	15500 8050 15200 8050
Wire Wire Line
	15200 8050 15200 7300
Wire Wire Line
	15500 8150 15100 8150
Wire Wire Line
	15100 8150 15100 7200
Wire Wire Line
	16500 7950 16600 7950
Wire Wire Line
	16600 7950 16600 7600
Wire Wire Line
	16500 8150 16700 8150
Wire Wire Line
	16700 8150 16700 7700
Wire Wire Line
	16500 8050 16800 8050
Wire Wire Line
	16500 7850 16800 7850
Wire Wire Line
	15500 8250 15000 8250
Connection ~ 15000 8250
$Comp
L PCA9615:PCA9615 U?
U 1 1 5F41504C
P 16000 8050
F 0 "U?" H 16000 7613 60  0000 C CNN
F 1 "PCA9615" H 16000 7719 60  0000 C CNN
F 2 "" H 16000 8050 60  0001 C CNN
F 3 "" H 16000 8050 60  0001 C CNN
	1    16000 8050
	-1   0    0    1   
$EndComp
Wire Wire Line
	16500 8250 16600 8250
Wire Wire Line
	15000 8400 16600 8400
Wire Wire Line
	16600 8250 16600 8400
Wire Wire Line
	16600 8400 16600 8850
Wire Wire Line
	16600 8850 15900 8850
Connection ~ 16600 8400
Wire Wire Line
	16600 8850 16600 9350
Wire Wire Line
	16600 9350 15900 9350
Connection ~ 16600 8850
Wire Wire Line
	15000 9350 15600 9350
Connection ~ 16800 8050
Wire Wire Line
	15000 8850 15000 9350
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EB288F7
P 15000 7200
F 0 "#SUPPLY?" H 15050 7200 45  0001 L BNN
F 1 "VCC" H 15000 7370 45  0000 C CNN
F 2 "" H 15000 7381 60  0000 C CNN
F 3 "" H 15000 7200 60  0001 C CNN
	1    15000 7200
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EB28D07
P 15000 10100
F 0 "#GND?" H 15050 10050 45  0001 L BNN
F 1 "GND" H 15000 9930 45  0000 C CNN
F 2 "" H 15000 10000 60  0001 C CNN
F 3 "" H 15000 10000 60  0001 C CNN
	1    15000 10100
	1    0    0    -1  
$EndComp
Text Notes 17400 5650 0    157  Italic 0
Differential I2C
Text Notes 750  6600 0    157  Italic 0
Power Monitor
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5F41506A
P -1650 3600
F 0 "R?" H -1650 3900 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 3816 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 3750 20  0001 C CNN
F 3 "" H -1650 3600 60  0001 C CNN
F 4 "RES-12183" H -1650 3721 60  0000 C CNN "Field4"
	1    -1650 3600
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5F41506B
P -1650 4000
F 0 "R?" H -1650 4300 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 4216 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 4150 20  0001 C CNN
F 3 "" H -1650 4000 60  0001 C CNN
F 4 "RES-12183" H -1650 4121 60  0000 C CNN "Field4"
	1    -1650 4000
	1    0    0    -1  
$EndComp
Text GLabel -1450 3600 2    47   Output ~ 0
MS1
Text GLabel -1450 4000 2    47   Output ~ 0
MS2
Wire Wire Line
	-2150 3600 -2150 4000
Wire Wire Line
	-2150 3200 -2150 3600
Text GLabel -2750 4100 2    47   Output ~ 0
SENSE1
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5F41506E
P -3350 4100
F 0 "R?" H -3350 4400 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H -3350 4316 45  0000 C CNN
F 2 "0805" H -3350 4250 20  0001 C CNN
F 3 "" H -3350 4100 60  0001 C CNN
F 4 "RES-08474" H -3350 4221 60  0000 C CNN "Field4"
	1    -3350 4100
	1    0    0    -1  
$EndComp
Text GLabel -2850 2900 2    47   Output ~ 0
RC1
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EBEB9C4
P -3450 2900
F 0 "R?" H -3450 3094 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H -3450 3010 45  0000 C CNN
F 2 "0603" H -3450 3050 20  0001 C CNN
F 3 "" H -3450 2900 60  0001 C CNN
	1    -3450 2900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EBED03C
P -3500 3350
F 0 "C?" V -3840 3400 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V -3756 3400 45  0000 C CNN
F 2 "0603" H -3500 3600 20  0001 C CNN
F 3 "" H -3500 3350 50  0001 C CNN
F 4 "CAP-09232" V -3661 3400 60  0000 C CNN "Field4"
	1    -3500 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	-3650 2900 -4000 2900
Wire Wire Line
	-4000 3350 -3600 3350
Wire Wire Line
	-3250 2900 -2950 2900
Wire Wire Line
	-2950 2900 -2950 3350
Wire Wire Line
	-2950 3350 -3300 3350
Wire Wire Line
	-3900 4100 -3550 4100
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415071
P -4000 3450
F 0 "#GND?" H -3950 3400 45  0001 L BNN
F 1 "GND" H -4000 3280 45  0000 C CNN
F 2 "" H -4000 3350 60  0001 C CNN
F 3 "" H -4000 3350 60  0001 C CNN
	1    -4000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2850 2900 -2950 2900
Connection ~ -2950 2900
Wire Wire Line
	-3150 4100 -2750 4100
Text GLabel -2750 4500 2    47   Output ~ 0
SENSE2
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EC4F9D1
P -3350 4500
F 0 "R?" H -3350 4800 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H -3350 4716 45  0000 C CNN
F 2 "0805" H -3350 4650 20  0001 C CNN
F 3 "" H -3350 4500 60  0001 C CNN
F 4 "RES-08474" H -3350 4621 60  0000 C CNN "Field4"
	1    -3350 4500
	1    0    0    -1  
$EndComp
Text GLabel -2850 2050 2    47   Output ~ 0
RC2
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EC4F9D8
P -3450 2050
F 0 "R?" H -3450 2244 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H -3450 2160 45  0000 C CNN
F 2 "0603" H -3450 2200 20  0001 C CNN
F 3 "" H -3450 2050 60  0001 C CNN
	1    -3450 2050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EC4F9DF
P -3500 2500
F 0 "C?" V -3840 2550 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V -3756 2550 45  0000 C CNN
F 2 "0603" H -3500 2750 20  0001 C CNN
F 3 "" H -3500 2500 50  0001 C CNN
F 4 "CAP-09232" V -3661 2550 60  0000 C CNN "Field4"
	1    -3500 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	-3650 2050 -4000 2050
Wire Wire Line
	-4000 2500 -3600 2500
Wire Wire Line
	-3250 2050 -2950 2050
Wire Wire Line
	-2950 2050 -2950 2500
Wire Wire Line
	-2950 2500 -3300 2500
Wire Wire Line
	-3900 4500 -3550 4500
Wire Wire Line
	-2850 2050 -2950 2050
Connection ~ -2950 2050
Wire Wire Line
	-3150 4500 -2750 4500
Text GLabel -900 4300 2    47   Output ~ 0
ENABLE
Connection ~ -4000 3350
Connection ~ -4000 2500
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5ED13BA2
P -3900 600
F 0 "#GND?" H -3850 550 45  0001 L BNN
F 1 "GND" H -3900 430 45  0000 C CNN
F 2 "" H -3900 500 60  0001 C CNN
F 3 "" H -3900 500 60  0001 C CNN
	1    -3900 600 
	-1   0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5F41507B
P -3900 400
F 0 "R?" V -3995 468 45  0000 L CNN
F 1 "10KOHM-0603-1_10W-1%" V -3911 468 45  0000 L CNN
F 2 "0603" H -3900 550 20  0001 C CNN
F 3 "" H -3900 400 60  0001 C CNN
F 4 "RES-00824" V -3816 468 60  0000 L CNN "Field4"
	1    -3900 400 
	0    1    -1   0   
$EndComp
$Comp
L SparkFun-Resistors:6.8KOHM-0603-1_10W-1% R?
U 1 1 5F41507C
P -3900 0
F 0 "R?" V -3805 -68 45  0000 R CNN
F 1 "6.8KOHM-0603-1_10W-1%" V -3889 -68 45  0000 R CNN
F 2 "0603" H -3900 150 20  0001 C CNN
F 3 "" H -3900 0   60  0001 C CNN
F 4 "RES-08597" V -3984 -68 60  0000 R CNN "Field4"
	1    -3900 0   
	0    -1   1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EE2A63F
P -3900 -200
F 0 "#SUPPLY?" H -3850 -200 45  0001 L BNN
F 1 "VCC" H -3900 -30 45  0000 C CNN
F 2 "" H -3900 -19 60  0000 C CNN
F 3 "" H -3900 -200 60  0001 C CNN
	1    -3900 -200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 8600 3750 8900
Wire Wire Line
	16900 7700 16700 7700
Wire Wire Line
	16600 7600 16900 7600
Wire Wire Line
	16900 7500 15400 7500
Wire Wire Line
	15300 7400 16900 7400
Wire Wire Line
	16900 7300 15200 7300
Wire Wire Line
	15100 7200 16900 7200
Wire Wire Line
	15000 8250 15000 7200
Wire Wire Line
	15000 8250 15000 8400
Wire Wire Line
	16800 7850 16800 8050
Wire Wire Line
	15000 8850 15600 8850
$Comp
L SparkFun-LED:LED-BLUE0603 D?
U 1 1 5F41507E
P 15450 9800
F 0 "D?" V 15055 9750 45  0000 C CNN
F 1 "LED-BLUE0603" V 15139 9750 45  0000 C CNN
F 2 "LED-0603" V 15750 9800 20  0001 C CNN
F 3 "" H 15450 9800 50  0001 C CNN
F 4 "DIO-08575" V 15234 9750 60  0000 C CNN "Field4"
	1    15450 9800
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5F41507F
P 16200 9800
F 0 "R?" H 16200 10100 45  0000 C CNN
F 1 "10KOHM-0603-1_10W-1%" H 16200 10016 45  0000 C CNN
F 2 "0603" H 16200 9950 20  0001 C CNN
F 3 "" H 16200 9800 60  0001 C CNN
F 4 "RES-00824" H 16200 9921 60  0000 C CNN "Field4"
	1    16200 9800
	1    0    0    -1  
$EndComp
Wire Wire Line
	16000 9800 15550 9800
Wire Wire Line
	16800 9800 16400 9800
Wire Wire Line
	16800 8050 16800 9800
Wire Wire Line
	15250 9800 15000 9800
Wire Wire Line
	15000 9800 15000 10100
Wire Wire Line
	15000 9350 15000 9800
Connection ~ 15000 9350
Connection ~ 15000 9800
Text GLabel 9700 1100 2    50   Output ~ 0
DSDA0_N
Text GLabel 9700 1000 2    50   Output ~ 0
DSDA0_P
Text GLabel 9700 800  2    50   Output ~ 0
DSCL0_P
Text GLabel 9700 900  2    50   Output ~ 0
DSCL0_N
$Comp
L SparkFun-Connectors:CONN_04SCREW J?
U 1 1 5F161A9A
P 9600 1100
F 0 "J?" H 9558 1710 45  0000 C CNN
F 1 "CONN_04SCREW" H 9558 1626 45  0000 C CNN
F 2 "SCREWTERMINAL-3.5MM-4" H 9600 1600 20  0001 C CNN
F 3 "" H 9600 1100 50  0001 C CNN
F 4 "2xCONN-08399" H 9558 1531 60  0000 C CNN "Field4"
	1    9600 1100
	1    0    0    -1  
$EndComp
Text GLabel -3850 200  2    48   Input ~ 0
PFD
Wire Wire Line
	-3850 200  -3900 200 
Connection ~ -3900 200 
$Comp
L SparkFun-IC-Microcontroller:ATMEGA32U4 U?
U 1 1 5F20C992
P 16200 3450
F 0 "U?" H 16668 5260 45  0000 C CNN
F 1 "ATMEGA32U4" H 16668 5176 45  0000 C CNN
F 2 "QFN-44" H 16200 5100 20  0001 C CNN
F 3 "" H 16200 3450 50  0001 C CNN
F 4 "IC-10828" H 16668 5081 60  0000 C CNN "Field4"
	1    16200 3450
	1    0    0    -1  
$EndComp
Text GLabel 16900 3200 2    50   Input ~ 0
SDA
Text GLabel 16900 3100 2    50   Input ~ 0
SCL
Text GLabel 15500 2000 0    47   Input ~ 0
RESET
Wire Wire Line
	3250 7200 3250 7450
Wire Wire Line
	3250 7450 950  7450
Wire Wire Line
	3250 7200 4050 7200
Connection ~ 950  7450
Wire Wire Line
	950  7450 950  8600
Wire Wire Line
	950  7100 950  7450
Text Notes 800  6800 0    47   ~ 0
Monitor the voltage and current and sends back\ninformation to master. Useful for logs
Text Notes -6300 -250 0    157  Italic 0
Motor Driver\n
Text GLabel -5900 2000 0    47   Output ~ 0
REF
$Comp
L dk_PMIC-Motor-Drivers-Controllers:A3967SLBTR-T U?
U 1 1 5EB6D200
P -5400 2600
F 0 "U?" H -5200 2350 60  0000 C CNN
F 1 "A3967SLBTR-T" V -4900 2150 60  0000 C CNN
F 2 "digikey-footprints:SOIC-24_W7.50mm" H -5200 2800 60  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H -5200 2900 60  0001 L CNN
F 4 "620-1140-1-ND" H -5200 3000 60  0001 L CNN "Digi-Key_PN"
F 5 "A3967SLBTR-T" H -5200 3100 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H -5200 3200 60  0001 L CNN "Category"
F 7 "PMIC - Motor Drivers, Controllers" H -5200 3300 60  0001 L CNN "Family"
F 8 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H -5200 3400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/allegro-microsystems-llc/A3967SLBTR-T/620-1140-1-ND/1090383" H -5200 3500 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MTR DRV BIPOLAR 3-5.5V 24SOIC" H -5200 3600 60  0001 L CNN "Description"
F 11 "Allegro MicroSystems, LLC" H -5200 3700 60  0001 L CNN "Manufacturer"
F 12 "Active" H -5200 3800 60  0001 L CNN "Status"
	1    -5400 2600
	1    0    0    -1  
$EndComp
Text GLabel -4900 2300 2    47   Output ~ 0
OUT1A
Text GLabel -4900 2400 2    47   Output ~ 0
OUT1B
Text GLabel -5900 2200 0    47   Input ~ 0
SLEEP
Text GLabel -5900 2700 0    47   Input ~ 0
MS2
Text GLabel -5900 3000 0    47   Input ~ 0
RESET
Text GLabel -5900 2100 0    47   Input ~ 0
RC2
Text GLabel -5900 3100 0    47   Input ~ 0
RC1
Text GLabel -5900 2900 0    47   Input ~ 0
SENSE1
Text GLabel -5900 2300 0    47   Input ~ 0
SENSE2
Text GLabel -5900 2600 0    47   Input ~ 0
MS1
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC39CD6
P -5500 3500
F 0 "#GND?" H -5450 3450 45  0001 L BNN
F 1 "GND" H -5500 3330 45  0000 C CNN
F 2 "" H -5500 3400 60  0001 C CNN
F 3 "" H -5500 3400 60  0001 C CNN
	1    -5500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	-5200 3400 -5200 3500
Wire Wire Line
	-5200 3500 -5300 3500
Wire Wire Line
	-5500 3400 -5500 3500
Connection ~ -5500 3500
Wire Wire Line
	-5400 3400 -5400 3500
Connection ~ -5400 3500
Wire Wire Line
	-5400 3500 -5500 3500
Wire Wire Line
	-5300 3400 -5300 3500
Connection ~ -5300 3500
Wire Wire Line
	-5300 3500 -5400 3500
Text GLabel -5900 2400 0    47   Input ~ 0
STEP
Text GLabel -5900 2500 0    47   Input ~ 0
DIR
Text GLabel -5900 2800 0    47   Input ~ 0
ENABLE
Text GLabel -5900 3200 0    48   Input ~ 0
PFD
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F56C56A
P -5300 1700
F 0 "#SUPPLY?" H -5250 1700 45  0001 L BNN
F 1 "VCC" H -5300 1870 45  0000 C CNN
F 2 "" H -5300 1881 60  0000 C CNN
F 3 "" H -5300 1700 60  0001 C CNN
	1    -5300 1700
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F56D965
P -5500 1700
F 0 "#SUPPLY?" H -5450 1700 45  0001 L BNN
F 1 "VOUT" H -5500 1870 45  0000 C CNN
F 2 "" H -5500 1881 60  0000 C CNN
F 3 "" H -5500 1700 60  0001 C CNN
	1    -5500 1700
	1    0    0    -1  
$EndComp
$Comp
L dk_PMIC-Voltage-Regulators-Linear:MCP1700-3302E_TO U?
U 1 1 5F41504E
P 11700 2750
F 0 "U?" H 11700 3037 60  0000 C CNN
F 1 "MCP1700-3302E_TO" H 11700 2931 60  0000 C CNN
F 2 "digikey-footprints:TO-92-3" H 11900 2950 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 11900 3050 60  0001 L CNN
F 4 "MCP1700-3302E/TO-ND" H 11900 3150 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP1700-3302E/TO" H 11900 3250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 11900 3350 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 11900 3450 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 11900 3550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP1700-3302E-TO/MCP1700-3302E-TO-ND/652680" H 11900 3650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 250MA TO92-3" H 11900 3750 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 11900 3850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11900 3950 60  0001 L CNN "Status"
	1    11700 2750
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9BFD41
P 11700 3050
F 0 "#GND?" H 11750 3000 45  0001 L BNN
F 1 "GND" H 11700 2880 45  0000 C CNN
F 2 "" H 11700 2950 60  0001 C CNN
F 3 "" H 11700 2950 60  0001 C CNN
	1    11700 3050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5E9C0E02
P 12100 2950
F 0 "C?" H 11992 2905 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 11992 2989 45  0000 R CNN
F 2 "0603" H 12100 3200 20  0001 C CNN
F 3 "" H 12100 2950 50  0001 C CNN
F 4 "CAP-00868" H 11992 3084 60  0000 R CNN "Field4"
	1    12100 2950
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5E9C596F
P 11150 2950
F 0 "C?" H 11043 2905 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 11043 2989 45  0000 R CNN
F 2 "0603" H 11150 3200 20  0001 C CNN
F 3 "" H 11150 2950 50  0001 C CNN
F 4 "CAP-00868" H 11043 3084 60  0000 R CNN "Field4"
	1    11150 2950
	1    0    0    1   
$EndComp
Wire Wire Line
	12100 2850 12100 2750
Wire Wire Line
	12100 2750 12000 2750
Wire Wire Line
	11400 2750 11150 2750
Wire Wire Line
	11150 2750 11150 2850
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9D5E7E
P 11150 3150
F 0 "#GND?" H 11200 3100 45  0001 L BNN
F 1 "GND" H 11150 2980 45  0000 C CNN
F 2 "" H 11150 3050 60  0001 C CNN
F 3 "" H 11150 3050 60  0001 C CNN
	1    11150 3150
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9DCFA8
P 12100 3150
F 0 "#GND?" H 12150 3100 45  0001 L BNN
F 1 "GND" H 12100 2980 45  0000 C CNN
F 2 "" H 12100 3050 60  0001 C CNN
F 3 "" H 12100 3050 60  0001 C CNN
	1    12100 3150
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E9E4534
P 11150 2600
F 0 "#SUPPLY?" H 11200 2600 45  0001 L BNN
F 1 "3.3V" H 11150 2770 45  0000 C CNN
F 2 "" H 11150 2781 60  0000 C CNN
F 3 "" H 11150 2600 60  0001 C CNN
	1    11150 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 2600 11150 2750
Connection ~ 11150 2750
$Comp
L SparkFun-Capacitors:47UF-POLAR-EIA3528-10V-10% C?
U 1 1 5F41505B
P -5450 1300
F 0 "C?" V -5090 1250 45  0000 C CNN
F 1 "47UF-POLAR-EIA3528-10V-10%" V -5174 1250 45  0000 C CNN
F 2 "EIA3528" H -5450 1550 20  0001 C CNN
F 3 "" H -5450 1300 50  0001 C CNN
F 4 "CAP-08310" V -5269 1250 60  0000 C CNN "Field4"
	1    -5450 1300
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F41505E
P -5250 1300
F 0 "#GND?" H -5200 1250 45  0001 L BNN
F 1 "GND" H -5250 1130 45  0000 C CNN
F 2 "" H -5250 1200 60  0001 C CNN
F 3 "" H -5250 1200 60  0001 C CNN
	1    -5250 1300
	0    -1   -1   0   
$EndComp
Text Notes -6100 4050 0    50   ~ 0
Star ground system located close \nto driver is recommended. The analog\n ground and power ground are \ninternally bonded. (PINS 6,7,18,19)
Connection ~ -4000 2900
Wire Wire Line
	-4000 2900 -4000 3350
Wire Wire Line
	-4000 3350 -4000 3450
Wire Wire Line
	-4000 2050 -4000 2500
Text Notes -4050 4950 0    50   ~ 0
To minimize voltage drop from current \nsensing to ground, use a low value \nresistor, connect to ground with individual \npaths as close as possible. 
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415060
P -3900 4100
F 0 "#GND?" H -3850 4050 45  0001 L BNN
F 1 "GND" H -3900 3930 45  0000 C CNN
F 2 "" H -3900 4000 60  0001 C CNN
F 3 "" H -3900 4000 60  0001 C CNN
	1    -3900 4100
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415061
P -3900 4500
F 0 "#GND?" H -3850 4450 45  0001 L BNN
F 1 "GND" H -3900 4330 45  0000 C CNN
F 2 "" H -3900 4400 60  0001 C CNN
F 3 "" H -3900 4400 60  0001 C CNN
	1    -3900 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	-4000 2500 -4000 2900
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EAA0AC9
P -2050 4300
F 0 "#GND?" H -2000 4250 45  0001 L BNN
F 1 "GND" H -2050 4130 45  0000 C CNN
F 2 "" H -2050 4200 60  0001 C CNN
F 3 "" H -2050 4200 60  0001 C CNN
	1    -2050 4300
	0    1    1    0   
$EndComp
Text Notes -2200 4650 0    50   ~ 0
When logic high, outputs are disabled.\nInputs (STEP, DIR, MS1, MS2) are still\naccessible.
Wire Wire Line
	-2050 4300 -900 4300
Connection ~ -2150 3600
Wire Wire Line
	-2150 2900 -2150 3200
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F41506D
P -2150 2900
F 0 "#SUPPLY?" H -2100 2900 45  0001 L BNN
F 1 "VCC" H -2150 3070 45  0000 C CNN
F 2 "" H -2150 3081 60  0000 C CNN
F 3 "" H -2150 2900 60  0001 C CNN
	1    -2150 2900
	1    0    0    -1  
$EndComp
Text GLabel -1450 3200 2    47   Output ~ 0
SLEEP
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB9178A
P -1650 3200
F 0 "R?" H -1650 3500 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 3416 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 3350 20  0001 C CNN
F 3 "" H -1650 3200 60  0001 C CNN
F 4 "RES-12183" H -1650 3321 60  0000 C CNN "Field4"
	1    -1650 3200
	1    0    0    -1  
$EndComp
Connection ~ -2150 3200
Wire Wire Line
	9800 2300 9800 2650
Wire Wire Line
	9500 3350 9500 3250
Wire Wire Line
	9400 3250 9400 3350
Wire Wire Line
	9400 3250 9500 3250
Wire Wire Line
	9600 2300 9600 2550
Text Notes -3800 1650 0    50   ~ 0
If V at PFD is greater than 0.6 VCC,\nslow decay mode is selected.\nAllows the motor is stop quickly.\n\nIf V at PFD is less than 0.21 VCC,\nfast decay mode is selected.\nResponds more quickly to changing \nstep inputs but stops slowly.\n\nElse, mixed decay mode is selected.\nAllows for microstepping to use the\nbest of both worlds. Mixed mode is\nwhat is selected here (0.4 VCC)
Wire Notes Line
	-4150 3750 -4150 5000
Wire Notes Line
	-2350 5000 -2350 3750
Wire Notes Line
	-4150 3750 -2350 3750
Wire Notes Line
	-4150 5000 -2350 5000
Wire Notes Line
	-2300 4150 -2300 4700
Wire Notes Line
	-500 4700 -500 4150
Wire Notes Line
	-2300 4150 -500 4150
Wire Notes Line
	-2300 4100 -2300 2600
Wire Notes Line
	-2300 4100 -500 4100
Wire Notes Line
	-2300 2600 -500 2600
Wire Notes Line
	-500 2600 -500 4100
Wire Wire Line
	-1850 3200 -2150 3200
Wire Wire Line
	-1850 3600 -2150 3600
Wire Wire Line
	-1850 4000 -2150 4000
Wire Notes Line
	-2300 -450 -500 -450
Wire Notes Line
	-500 900  -500 -450
Wire Notes Line
	-2300 900  -500 900 
Wire Notes Line
	-2300 -450 -2300 900 
Text Notes -1950 750  0    50   ~ 0
Active Low sets the translator to \npredefined home state:\n(45 Step Angle, DIR = H)\nSTEP inputs are ignored untill \nRESET goes high.
Connection ~ -2050 -50 
Wire Wire Line
	-2050 -200 -2050 -50 
Wire Wire Line
	-2050 450  -2050 650 
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EB2F983
P -2050 650
F 0 "#GND?" H -2000 600 45  0001 L BNN
F 1 "GND" H -2050 480 45  0000 C CNN
F 2 "" H -2050 550 60  0001 C CNN
F 3 "" H -2050 550 60  0001 C CNN
	1    -2050 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	-2050 50   -2050 -50 
$Comp
L dk_Pushbutton-Switches:GPTS203211B S?
U 1 1 5F415064
P -2050 250
F 0 "S?" V -2004 206 50  0000 R CNN
F 1 "GPTS203211B" V -2095 206 50  0000 R CNN
F 2 "digikey-footprints:PushButton_12x12mm_THT_GPTS203211B" H -1850 450 50  0001 L CNN
F 3 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H -1850 550 60  0001 L CNN
F 4 "CW181-ND" H -1850 650 60  0001 L CNN "Digi-Key_PN"
F 5 "GPTS203211B" H -1850 750 60  0001 L CNN "MPN"
F 6 "Switches" H -1850 850 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H -1850 950 60  0001 L CNN "Family"
F 8 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H -1850 1050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cw-industries/GPTS203211B/CW181-ND/3190590" H -1850 1150 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSHBUTTON SPST 1A 30V" H -1850 1250 60  0001 L CNN "Description"
F 11 "CW Industries" H -1850 1350 60  0001 L CNN "Manufacturer"
F 12 "Active" H -1850 1450 60  0001 L CNN "Status"
	1    -2050 250 
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F415063
P -2050 -200
F 0 "#SUPPLY?" H -2000 -200 45  0001 L BNN
F 1 "VCC" H -2050 -30 45  0000 C CNN
F 2 "" H -2050 -19 60  0000 C CNN
F 3 "" H -2050 -200 60  0001 C CNN
	1    -2050 -200
	1    0    0    -1  
$EndComp
Text GLabel -4900 2600 2    47   Output ~ 0
OUT2B
Text GLabel -4900 2500 2    47   Output ~ 0
OUT2A
Wire Wire Line
	-2050 -50  -1650 -50 
Text GLabel -1250 -50  2    47   Output ~ 0
RESET
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5F41506C
P -1450 -50
F 0 "R?" H -1450 250 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1450 166 45  0000 C CNN
F 2 "AXIAL-0.3" H -1450 100 20  0001 C CNN
F 3 "" H -1450 -50 60  0001 C CNN
F 4 "RES-12183" H -1450 71  60  0000 C CNN "Field4"
	1    -1450 -50 
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415079
P -2000 2300
F 0 "#GND?" H -1950 2250 45  0001 L BNN
F 1 "GND" H -2000 2130 45  0000 C CNN
F 2 "" H -2000 2200 60  0001 C CNN
F 3 "" H -2000 2200 60  0001 C CNN
	1    -2000 2300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:POTENTIOMETER-PTH-9MM-1_20W-20% VR?
U 1 1 5EC83193
P -2000 1600
F 0 "VR?" H -2068 1505 45  0000 R CNN
F 1 "POTENTIOMETER-PTH-9MM-1_20W-20%" H -2068 1589 45  0000 R CNN
F 2 "POT-PTH-ALPS" V -2150 1600 20  0001 C CNN
F 3 "" H -2000 1600 60  0001 C CNN
F 4 "RES-09177" H -2068 1684 60  0000 R CNN "Field4"
	1    -2000 1600
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Resistors:2.49KOHM-0603-1_10W-1% R?
U 1 1 5EC823D7
P -2000 2100
F 0 "R?" V -2095 2168 45  0000 L CNN
F 1 "2.49KOHM-0603-1_10W-1%" V -2011 2168 45  0000 L CNN
F 2 "0603" H -2000 2250 20  0001 C CNN
F 3 "" H -2000 2100 60  0001 C CNN
F 4 "RES-09568" V -1916 2168 60  0000 L CNN "Field4"
	1    -2000 2100
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F415076
P -2000 1300
F 0 "#SUPPLY?" H -1950 1300 45  0001 L BNN
F 1 "VCC" H -2000 1470 45  0000 C CNN
F 2 "" H -2000 1481 60  0000 C CNN
F 3 "" H -2000 1300 60  0001 C CNN
	1    -2000 1300
	1    0    0    -1  
$EndComp
Text Notes -1900 1400 0    47   ~ 0
At REF of 5V -> 833mA/phase\nAt REF of 2V -> 333mA/phase\n\nMinimum current = smootest steps\nMaximum current = highest torque
Text GLabel -2200 1550 1    47   Output ~ 0
REF
Wire Wire Line
	-2200 1600 -2200 1550
Wire Notes Line
	-2300 950  -2300 2550
Wire Notes Line
	-2300 2550 -500 2550
Wire Notes Line
	-500 2550 -500 950 
Wire Notes Line
	-2300 950  -500 950 
Wire Notes Line
	-2300 4700 -500 4700
Wire Wire Line
	-5300 1700 -5300 1800
Wire Wire Line
	-5500 1700 -5400 1700
Wire Wire Line
	-5400 1700 -5400 1800
Wire Wire Line
	-5500 1700 -5500 1800
Connection ~ -5500 1700
Wire Notes Line
	-4150 3700 -2350 3700
Wire Notes Line
	-2350 3700 -2350 1750
Wire Notes Line
	-2350 1750 -4150 1750
Wire Notes Line
	-4150 1750 -4150 3700
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F10EFA5
P -5550 1300
F 0 "#SUPPLY?" H -5500 1300 45  0001 L BNN
F 1 "VOUT" H -5550 1470 45  0000 C CNN
F 2 "" H -5550 1481 60  0000 C CNN
F 3 "" H -5550 1300 60  0001 C CNN
	1    -5550 1300
	0    -1   -1   0   
$EndComp
Text Notes -6250 650  0    50   ~ 0
+- 750mA, 30V output rating\nMin 4.75V load voltage\n3.0 - 5.5 V logic supply\nCrossover protection\nMixed, fast, & slow decay modes\n
Text Notes -6250 100  0    50   ~ 0
Allegro A3967 Microstepping Driver with translator\n\nSimilar to what EasyDriver uses. Nifty little IC that\ntakes care of almost all the logic of it. 
Wire Notes Line
	-2350 1700 -2350 -450
Wire Notes Line
	-2350 -450 -4150 -450
Wire Notes Line
	-4150 -450 -4150 1700
Wire Notes Line
	-4150 1700 -2350 1700
Wire Notes Line
	500  6100 7750 6100
Text Notes 17400 6950 0    50   ~ 0
Inter-Integrated Circuit (I2C)\n\nSDA: Serial Data, used to send and receive data\nSCL: Serial Clock, used to carry clock signal\n\nSynchronous protocol with only two wires. \nUsed quite a bit to interface with sensors.\n\nUnfortunately, two conductive wires that extend for some \ndistance acts as a "bus capacitor" that  degrades signal \nquality. Using stronger pullup resistors can charge that \nparasitic capacitor quicker but also means the higher current \nthat flows through your fragile sensors, which places a hard \nlimit on such finesses. Using a differential pair helps reduce \nthe parastic losses and is more able to handle EMI.
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E98AA3E
P 10300 2250
F 0 "#GND?" H 10350 2200 45  0001 L BNN
F 1 "GND" H 10300 2080 45  0000 C CNN
F 2 "" H 10300 2150 60  0001 C CNN
F 3 "" H 10300 2150 60  0001 C CNN
	1    10300 2250
	1    0    0    -1  
$EndComp
Text GLabel 12350 1900 0    50   Input ~ 0
DSCL0_N
Text GLabel 12350 1500 0    50   Input ~ 0
DSCL0_P
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E992798
P 12500 2300
F 0 "#GND?" H 12550 2250 45  0001 L BNN
F 1 "GND" H 12500 2130 45  0000 C CNN
F 2 "" H 12500 2200 60  0001 C CNN
F 3 "" H 12500 2200 60  0001 C CNN
	1    12500 2300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F415048
P 12500 1300
F 0 "R?" V 12405 1368 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 1368 45  0000 L CNN
F 2 "0603" H 12500 1450 20  0001 C CNN
F 3 "" H 12500 1300 60  0001 C CNN
F 4 "RES-07857" V 12584 1368 60  0000 L CNN "Field4"
	1    12500 1300
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F415049
P 12500 1700
F 0 "R?" V 12405 1768 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 1768 45  0000 L CNN
F 2 "0603" H 12500 1850 20  0001 C CNN
F 3 "" H 12500 1700 60  0001 C CNN
F 4 "RES-07857" V 12584 1768 60  0000 L CNN "Field4"
	1    12500 1700
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F41504A
P 12500 2100
F 0 "R?" V 12405 2168 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12489 2168 45  0000 L CNN
F 2 "0603" H 12500 2250 20  0001 C CNN
F 3 "" H 12500 2100 60  0001 C CNN
F 4 "RES-07857" V 12584 2168 60  0000 L CNN "Field4"
	1    12500 2100
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5F41504B
P 12500 1100
F 0 "#SUPPLY?" H 12550 1100 45  0001 L BNN
F 1 "3.3V" H 12500 1270 45  0000 C CNN
F 2 "" H 12500 1281 60  0000 C CNN
F 3 "" H 12500 1100 60  0001 C CNN
	1    12500 1100
	1    0    0    -1  
$EndComp
Text GLabel 12650 1500 2    50   Output ~ 0
DSCLP
Text GLabel 12650 1900 2    50   Output ~ 0
DSCLM
Text GLabel 16900 7400 2    50   Input ~ 0
DSDAP
Text GLabel 16900 7500 2    50   Input ~ 0
DSDAM
Text GLabel 16900 7300 2    50   Input ~ 0
DSCLP
Text GLabel 16900 7200 2    50   Input ~ 0
DSCLM
Text GLabel 16900 7600 2    50   Output ~ 0
SCL
Text GLabel 16900 7700 2    50   Output ~ 0
SDA
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5E9A3AAE
P 15800 9350
F 0 "C?" V 16140 9400 45  0000 C CNN
F 1 "0.1UF-0603-100V-10%" V 16056 9400 45  0000 C CNN
F 2 "0603" H 15800 9600 20  0001 C CNN
F 3 "" H 15800 9350 50  0001 C CNN
F 4 "CAP-08390" V 15961 9400 60  0000 C CNN "Field4"
	1    15800 9350
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-Capacitors:100UF-POLAR-10X10.5-63V-20% C?
U 1 1 5F41504F
P 15800 8850
F 0 "C?" V 15440 8800 45  0000 C CNN
F 1 "100UF-POLAR-10X10.5-63V-20%" V 15524 8800 45  0000 C CNN
F 2 "NIC_10X10.5_CAP" H 15800 9100 20  0001 C CNN
F 3 "" H 15800 8850 50  0001 C CNN
F 4 "CAP-08362" V 15619 8800 60  0000 C CNN "Field4"
	1    15800 8850
	0    1    1    0   
$EndComp
Text GLabel 10450 1450 2    50   Output ~ 0
DSDAP
Text GLabel 10450 1850 2    50   Output ~ 0
DSDAM
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F415046
P 10300 2050
F 0 "R?" V 10205 2118 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 2118 45  0000 L CNN
F 2 "0603" H 10300 2200 20  0001 C CNN
F 3 "" H 10300 2050 60  0001 C CNN
F 4 "RES-07857" V 10384 2118 60  0000 L CNN "Field4"
	1    10300 2050
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F415045
P 10300 1650
F 0 "R?" V 10205 1718 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 1718 45  0000 L CNN
F 2 "0603" H 10300 1800 20  0001 C CNN
F 3 "" H 10300 1650 60  0001 C CNN
F 4 "RES-07857" V 10384 1718 60  0000 L CNN "Field4"
	1    10300 1650
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5F415044
P 10300 1250
F 0 "R?" V 10205 1318 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10289 1318 45  0000 L CNN
F 2 "0603" H 10300 1400 20  0001 C CNN
F 3 "" H 10300 1250 60  0001 C CNN
F 4 "RES-07857" V 10384 1318 60  0000 L CNN "Field4"
	1    10300 1250
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5F415042
P 10300 1050
F 0 "#SUPPLY?" H 10350 1050 45  0001 L BNN
F 1 "3.3V" H 10300 1220 45  0000 C CNN
F 2 "" H 10300 1231 60  0000 C CNN
F 3 "" H 10300 1050 60  0001 C CNN
	1    10300 1050
	1    0    0    -1  
$EndComp
Text GLabel 10150 1450 0    50   Input ~ 0
DSDA0_P
Text GLabel 10150 1850 0    50   Input ~ 0
DSDA0_N
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5F415054
P 2350 8900
F 0 "JP?" H 2454 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 9150 20  0001 C CNN
F 3 "" H 2350 8900 60  0001 C CNN
F 4 "" H 2454 8816 60  0000 L CNN "PROD_ID"
	1    2350 8900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_1-NC JP?
U 1 1 5E9CA69F
P 1850 8900
F 0 "JP?" H 1954 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_1-NC" H 1954 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_1-NC" H 1850 9150 20  0001 C CNN
F 3 "" V 1850 8845 60  0001 C CNN
F 4 "" H 1954 8984 60  0000 L CNN "PROD_ID"
	1    1850 8900
	-1   0    0    1   
$EndComp
$Comp
L dk_PMIC-Current-Regulation-Management:INA219AIDCNR U?
U 1 1 5F41505A
P 4050 8100
F 0 "U?" H 4178 8103 60  0000 L CNN
F 1 "INA219AIDCNR" H 4178 7997 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-8" H 4250 8300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8400 60  0001 L CNN
F 4 "296-23770-1-ND" H 4250 8500 60  0001 L CNN "Digi-Key_PN"
F 5 "INA219AIDCNR" H 4250 8600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4250 8700 60  0001 L CNN "Category"
F 7 "PMIC - Current Regulation/Management" H 4250 8800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/INA219AIDCNR/296-23770-1-ND/1952550" H 4250 9000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC CURRENT MONITOR 1% SOT23-8" H 4250 9100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4250 9200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4250 9300 60  0001 L CNN "Status"
	1    4050 8100
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5F415055
P 1850 7850
F 0 "JP?" H 1954 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1954 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 1850 8100 20  0001 C CNN
F 3 "" H 1850 7850 60  0001 C CNN
F 4 "" H 1954 7766 60  0000 L CNN "PROD_ID"
	1    1850 7850
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5F415052
P 2350 7850
F 0 "JP?" H 2454 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 8100 20  0001 C CNN
F 3 "" H 2350 7850 60  0001 C CNN
F 4 "" H 2454 7766 60  0000 L CNN "PROD_ID"
	1    2350 7850
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EA298B3
P 950 7100
F 0 "#SUPPLY?" H 1000 7100 45  0001 L BNN
F 1 "VCC" H 950 7270 45  0000 C CNN
F 2 "" H 950 7281 60  0000 C CNN
F 3 "" H 950 7100 60  0001 C CNN
	1    950  7100
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5F41505D
P 3750 9000
F 0 "C?" H 3858 9145 45  0000 L CNN
F 1 "0.1UF-0603-100V-10%" H 3858 9061 45  0000 L CNN
F 2 "0603" H 3750 9250 20  0001 C CNN
F 3 "" H 3750 9000 50  0001 C CNN
F 4 "CAP-08390" H 3858 8966 60  0000 L CNN "Field4"
	1    3750 9000
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F41505F
P 950 9350
F 0 "#GND?" H 1000 9300 45  0001 L BNN
F 1 "GND" H 950 9180 45  0000 C CNN
F 2 "" H 950 9250 60  0001 C CNN
F 3 "" H 950 9250 60  0001 C CNN
	1    950  9350
	1    0    0    -1  
$EndComp
Text Notes 1100 8400 0    50   ~ 0
I2C Address Selection
Text GLabel 4500 7600 2    50   Output ~ 0
IN+
Text GLabel 4500 7500 2    50   Output ~ 0
IN-
Text GLabel 4500 7400 2    50   BiDi ~ 0
SDA
Text GLabel 4500 7300 2    50   BiDi ~ 0
SCL
$Comp
L PCA9615:PCA9615 U?
U 1 1 5E99CC70
P 16000 8050
F 0 "U?" H 16000 7613 60  0000 C CNN
F 1 "PCA9615" H 16000 7719 60  0000 C CNN
F 2 "" H 16000 8050 60  0001 C CNN
F 3 "" H 16000 8050 60  0001 C CNN
	1    16000 8050
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F415065
P 15000 7200
F 0 "#SUPPLY?" H 15050 7200 45  0001 L BNN
F 1 "VCC" H 15000 7370 45  0000 C CNN
F 2 "" H 15000 7381 60  0000 C CNN
F 3 "" H 15000 7200 60  0001 C CNN
	1    15000 7200
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415066
P 15000 10100
F 0 "#GND?" H 15050 10050 45  0001 L BNN
F 1 "GND" H 15000 9930 45  0000 C CNN
F 2 "" H 15000 10000 60  0001 C CNN
F 3 "" H 15000 10000 60  0001 C CNN
	1    15000 10100
	1    0    0    -1  
$EndComp
Text Notes 17400 5650 0    157  Italic 0
Differential I2C
Text Notes 750  6600 0    157  Italic 0
Power Monitor
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB91C0D
P -1650 3600
F 0 "R?" H -1650 3900 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 3816 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 3750 20  0001 C CNN
F 3 "" H -1650 3600 60  0001 C CNN
F 4 "RES-12183" H -1650 3721 60  0000 C CNN "Field4"
	1    -1650 3600
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB950B0
P -1650 4000
F 0 "R?" H -1650 4300 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 4216 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 4150 20  0001 C CNN
F 3 "" H -1650 4000 60  0001 C CNN
F 4 "RES-12183" H -1650 4121 60  0000 C CNN "Field4"
	1    -1650 4000
	1    0    0    -1  
$EndComp
Text GLabel -1450 3600 2    47   Output ~ 0
MS1
Text GLabel -1450 4000 2    47   Output ~ 0
MS2
Text GLabel -2750 4100 2    47   Output ~ 0
SENSE1
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EBDB29D
P -3350 4100
F 0 "R?" H -3350 4400 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H -3350 4316 45  0000 C CNN
F 2 "0805" H -3350 4250 20  0001 C CNN
F 3 "" H -3350 4100 60  0001 C CNN
F 4 "RES-08474" H -3350 4221 60  0000 C CNN "Field4"
	1    -3350 4100
	1    0    0    -1  
$EndComp
Text GLabel -2850 2900 2    47   Output ~ 0
RC1
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5F41506F
P -3450 2900
F 0 "R?" H -3450 3094 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H -3450 3010 45  0000 C CNN
F 2 "0603" H -3450 3050 20  0001 C CNN
F 3 "" H -3450 2900 60  0001 C CNN
	1    -3450 2900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5F415070
P -3500 3350
F 0 "C?" V -3840 3400 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V -3756 3400 45  0000 C CNN
F 2 "0603" H -3500 3600 20  0001 C CNN
F 3 "" H -3500 3350 50  0001 C CNN
F 4 "CAP-09232" V -3661 3400 60  0000 C CNN "Field4"
	1    -3500 3350
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC2F662
P -4000 3450
F 0 "#GND?" H -3950 3400 45  0001 L BNN
F 1 "GND" H -4000 3280 45  0000 C CNN
F 2 "" H -4000 3350 60  0001 C CNN
F 3 "" H -4000 3350 60  0001 C CNN
	1    -4000 3450
	1    0    0    -1  
$EndComp
Text GLabel -2750 4500 2    47   Output ~ 0
SENSE2
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5F415073
P -3350 4500
F 0 "R?" H -3350 4800 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H -3350 4716 45  0000 C CNN
F 2 "0805" H -3350 4650 20  0001 C CNN
F 3 "" H -3350 4500 60  0001 C CNN
F 4 "RES-08474" H -3350 4621 60  0000 C CNN "Field4"
	1    -3350 4500
	1    0    0    -1  
$EndComp
Text GLabel -2850 2050 2    47   Output ~ 0
RC2
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5F415074
P -3450 2050
F 0 "R?" H -3450 2244 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H -3450 2160 45  0000 C CNN
F 2 "0603" H -3450 2200 20  0001 C CNN
F 3 "" H -3450 2050 60  0001 C CNN
	1    -3450 2050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5F415075
P -3500 2500
F 0 "C?" V -3840 2550 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V -3756 2550 45  0000 C CNN
F 2 "0603" H -3500 2750 20  0001 C CNN
F 3 "" H -3500 2500 50  0001 C CNN
F 4 "CAP-09232" V -3661 2550 60  0000 C CNN "Field4"
	1    -3500 2500
	0    1    1    0   
$EndComp
Text GLabel -900 4300 2    47   Output ~ 0
ENABLE
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F41507A
P -3900 600
F 0 "#GND?" H -3850 550 45  0001 L BNN
F 1 "GND" H -3900 430 45  0000 C CNN
F 2 "" H -3900 500 60  0001 C CNN
F 3 "" H -3900 500 60  0001 C CNN
	1    -3900 600 
	-1   0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5ED14FB6
P -3900 400
F 0 "R?" V -3995 468 45  0000 L CNN
F 1 "10KOHM-0603-1_10W-1%" V -3911 468 45  0000 L CNN
F 2 "0603" H -3900 550 20  0001 C CNN
F 3 "" H -3900 400 60  0001 C CNN
F 4 "RES-00824" V -3816 468 60  0000 L CNN "Field4"
	1    -3900 400 
	0    1    -1   0   
$EndComp
$Comp
L SparkFun-Resistors:6.8KOHM-0603-1_10W-1% R?
U 1 1 5ED162DB
P -3900 0
F 0 "R?" V -3805 -68 45  0000 R CNN
F 1 "6.8KOHM-0603-1_10W-1%" V -3889 -68 45  0000 R CNN
F 2 "0603" H -3900 150 20  0001 C CNN
F 3 "" H -3900 0   60  0001 C CNN
F 4 "RES-08597" V -3984 -68 60  0000 R CNN "Field4"
	1    -3900 0   
	0    -1   1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F41507D
P -3900 -200
F 0 "#SUPPLY?" H -3850 -200 45  0001 L BNN
F 1 "VCC" H -3900 -30 45  0000 C CNN
F 2 "" H -3900 -19 60  0000 C CNN
F 3 "" H -3900 -200 60  0001 C CNN
	1    -3900 -200
	-1   0    0    -1  
$EndComp
$Comp
L SparkFun-LED:LED-BLUE0603 D?
U 1 1 5F0F44C8
P 15450 9800
F 0 "D?" V 15055 9750 45  0000 C CNN
F 1 "LED-BLUE0603" V 15139 9750 45  0000 C CNN
F 2 "LED-0603" V 15750 9800 20  0001 C CNN
F 3 "" H 15450 9800 50  0001 C CNN
F 4 "DIO-08575" V 15234 9750 60  0000 C CNN "Field4"
	1    15450 9800
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5F0F6136
P 16200 9800
F 0 "R?" H 16200 10100 45  0000 C CNN
F 1 "10KOHM-0603-1_10W-1%" H 16200 10016 45  0000 C CNN
F 2 "0603" H 16200 9950 20  0001 C CNN
F 3 "" H 16200 9800 60  0001 C CNN
F 4 "RES-00824" H 16200 9921 60  0000 C CNN "Field4"
	1    16200 9800
	1    0    0    -1  
$EndComp
Text GLabel 9700 1100 2    50   Output ~ 0
DSDA0_N
Text GLabel 9700 1000 2    50   Output ~ 0
DSDA0_P
Text GLabel 9700 800  2    50   Output ~ 0
DSCL0_P
Text GLabel 9700 900  2    50   Output ~ 0
DSCL0_N
$Comp
L SparkFun-Connectors:CONN_04SCREW J?
U 1 1 5F415081
P 9600 1100
F 0 "J?" H 9558 1710 45  0000 C CNN
F 1 "CONN_04SCREW" H 9558 1626 45  0000 C CNN
F 2 "SCREWTERMINAL-3.5MM-4" H 9600 1600 20  0001 C CNN
F 3 "" H 9600 1100 50  0001 C CNN
F 4 "2xCONN-08399" H 9558 1531 60  0000 C CNN "Field4"
	1    9600 1100
	1    0    0    -1  
$EndComp
Text GLabel -3850 200  2    48   Input ~ 0
PFD
$Comp
L SparkFun-IC-Microcontroller:ATMEGA32U4 U?
U 1 1 5F415082
P 16200 3450
F 0 "U?" H 16668 5260 45  0000 C CNN
F 1 "ATMEGA32U4" H 16668 5176 45  0000 C CNN
F 2 "QFN-44" H 16200 5100 20  0001 C CNN
F 3 "" H 16200 3450 50  0001 C CNN
F 4 "IC-10828" H 16668 5081 60  0000 C CNN "Field4"
	1    16200 3450
	1    0    0    -1  
$EndComp
Text GLabel 16900 3200 2    50   Input ~ 0
SDA
Text GLabel 16900 3100 2    50   Input ~ 0
SCL
Text GLabel 15500 2000 0    47   Input ~ 0
RESET
Text Notes 800  6800 0    47   ~ 0
Monitor the voltage and current and sends back\ninformation to master. Useful for logs
Text Notes -6300 -250 0    157  Italic 0
Motor Driver\n
Text GLabel -5900 2000 0    47   Output ~ 0
REF
$Comp
L dk_PMIC-Motor-Drivers-Controllers:A3967SLBTR-T U?
U 1 1 5F415068
P -5400 2600
F 0 "U?" H -5200 2350 60  0000 C CNN
F 1 "A3967SLBTR-T" V -4900 2150 60  0000 C CNN
F 2 "digikey-footprints:SOIC-24_W7.50mm" H -5200 2800 60  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H -5200 2900 60  0001 L CNN
F 4 "620-1140-1-ND" H -5200 3000 60  0001 L CNN "Digi-Key_PN"
F 5 "A3967SLBTR-T" H -5200 3100 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H -5200 3200 60  0001 L CNN "Category"
F 7 "PMIC - Motor Drivers, Controllers" H -5200 3300 60  0001 L CNN "Family"
F 8 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H -5200 3400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/allegro-microsystems-llc/A3967SLBTR-T/620-1140-1-ND/1090383" H -5200 3500 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MTR DRV BIPOLAR 3-5.5V 24SOIC" H -5200 3600 60  0001 L CNN "Description"
F 11 "Allegro MicroSystems, LLC" H -5200 3700 60  0001 L CNN "Manufacturer"
F 12 "Active" H -5200 3800 60  0001 L CNN "Status"
	1    -5400 2600
	1    0    0    -1  
$EndComp
Text GLabel -4900 2300 2    47   Output ~ 0
OUT1A
Text GLabel -4900 2400 2    47   Output ~ 0
OUT1B
Text GLabel -5900 2200 0    47   Input ~ 0
SLEEP
Text GLabel -5900 2700 0    47   Input ~ 0
MS2
Text GLabel -5900 3000 0    47   Input ~ 0
RESET
Text GLabel -5900 2100 0    47   Input ~ 0
RC2
Text GLabel -5900 3100 0    47   Input ~ 0
RC1
Text GLabel -5900 2900 0    47   Input ~ 0
SENSE1
Text GLabel -5900 2300 0    47   Input ~ 0
SENSE2
Text GLabel -5900 2600 0    47   Input ~ 0
MS1
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415072
P -5500 3500
F 0 "#GND?" H -5450 3450 45  0001 L BNN
F 1 "GND" H -5500 3330 45  0000 C CNN
F 2 "" H -5500 3400 60  0001 C CNN
F 3 "" H -5500 3400 60  0001 C CNN
	1    -5500 3500
	1    0    0    -1  
$EndComp
Text GLabel -5900 2400 0    47   Input ~ 0
STEP
Text GLabel -5900 2500 0    47   Input ~ 0
DIR
Text GLabel -5900 2800 0    47   Input ~ 0
ENABLE
Text GLabel -5900 3200 0    48   Input ~ 0
PFD
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F415083
P -5300 1700
F 0 "#SUPPLY?" H -5250 1700 45  0001 L BNN
F 1 "VCC" H -5300 1870 45  0000 C CNN
F 2 "" H -5300 1881 60  0000 C CNN
F 3 "" H -5300 1700 60  0001 C CNN
	1    -5300 1700
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F415084
P -5500 1700
F 0 "#SUPPLY?" H -5450 1700 45  0001 L BNN
F 1 "VOUT" H -5500 1870 45  0000 C CNN
F 2 "" H -5500 1881 60  0000 C CNN
F 3 "" H -5500 1700 60  0001 C CNN
	1    -5500 1700
	1    0    0    -1  
$EndComp
$Comp
L dk_PMIC-Voltage-Regulators-Linear:MCP1700-3302E_TO U?
U 1 1 5E9A491C
P 11700 2750
F 0 "U?" H 11700 3037 60  0000 C CNN
F 1 "MCP1700-3302E_TO" H 11700 2931 60  0000 C CNN
F 2 "digikey-footprints:TO-92-3" H 11900 2950 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 11900 3050 60  0001 L CNN
F 4 "MCP1700-3302E/TO-ND" H 11900 3150 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP1700-3302E/TO" H 11900 3250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 11900 3350 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 11900 3450 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 11900 3550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP1700-3302E-TO/MCP1700-3302E-TO-ND/652680" H 11900 3650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 250MA TO92-3" H 11900 3750 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 11900 3850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11900 3950 60  0001 L CNN "Status"
	1    11700 2750
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415050
P 11700 3050
F 0 "#GND?" H 11750 3000 45  0001 L BNN
F 1 "GND" H 11700 2880 45  0000 C CNN
F 2 "" H 11700 2950 60  0001 C CNN
F 3 "" H 11700 2950 60  0001 C CNN
	1    11700 3050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5F415051
P 12100 2950
F 0 "C?" H 11992 2905 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 11992 2989 45  0000 R CNN
F 2 "0603" H 12100 3200 20  0001 C CNN
F 3 "" H 12100 2950 50  0001 C CNN
F 4 "CAP-00868" H 11992 3084 60  0000 R CNN "Field4"
	1    12100 2950
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5F415053
P 11150 2950
F 0 "C?" H 11043 2905 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 11043 2989 45  0000 R CNN
F 2 "0603" H 11150 3200 20  0001 C CNN
F 3 "" H 11150 2950 50  0001 C CNN
F 4 "CAP-00868" H 11043 3084 60  0000 R CNN "Field4"
	1    11150 2950
	1    0    0    1   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415057
P 11150 3150
F 0 "#GND?" H 11200 3100 45  0001 L BNN
F 1 "GND" H 11150 2980 45  0000 C CNN
F 2 "" H 11150 3050 60  0001 C CNN
F 3 "" H 11150 3050 60  0001 C CNN
	1    11150 3150
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415058
P 12100 3150
F 0 "#GND?" H 12150 3100 45  0001 L BNN
F 1 "GND" H 12100 2980 45  0000 C CNN
F 2 "" H 12100 3050 60  0001 C CNN
F 3 "" H 12100 3050 60  0001 C CNN
	1    12100 3150
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5F415059
P 11150 2600
F 0 "#SUPPLY?" H 11200 2600 45  0001 L BNN
F 1 "3.3V" H 11150 2770 45  0000 C CNN
F 2 "" H 11150 2781 60  0000 C CNN
F 3 "" H 11150 2600 60  0001 C CNN
	1    11150 2600
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:47UF-POLAR-EIA3528-10V-10% C?
U 1 1 5EA1B43D
P -5450 1300
F 0 "C?" V -5090 1250 45  0000 C CNN
F 1 "47UF-POLAR-EIA3528-10V-10%" V -5174 1250 45  0000 C CNN
F 2 "EIA3528" H -5450 1550 20  0001 C CNN
F 3 "" H -5450 1300 50  0001 C CNN
F 4 "CAP-08310" V -5269 1250 60  0000 C CNN "Field4"
	1    -5450 1300
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA3D799
P -5250 1300
F 0 "#GND?" H -5200 1250 45  0001 L BNN
F 1 "GND" H -5250 1130 45  0000 C CNN
F 2 "" H -5250 1200 60  0001 C CNN
F 3 "" H -5250 1200 60  0001 C CNN
	1    -5250 1300
	0    -1   -1   0   
$EndComp
Text Notes -6100 4050 0    50   ~ 0
Star ground system located close \nto driver is recommended. The analog\n ground and power ground are \ninternally bonded. (PINS 6,7,18,19)
Text Notes -4050 4950 0    50   ~ 0
To minimize voltage drop from current \nsensing to ground, use a low value \nresistor, connect to ground with individual \npaths as close as possible. 
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA75FCF
P -3900 4100
F 0 "#GND?" H -3850 4050 45  0001 L BNN
F 1 "GND" H -3900 3930 45  0000 C CNN
F 2 "" H -3900 4000 60  0001 C CNN
F 3 "" H -3900 4000 60  0001 C CNN
	1    -3900 4100
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA7D426
P -3900 4500
F 0 "#GND?" H -3850 4450 45  0001 L BNN
F 1 "GND" H -3900 4330 45  0000 C CNN
F 2 "" H -3900 4400 60  0001 C CNN
F 3 "" H -3900 4400 60  0001 C CNN
	1    -3900 4500
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415062
P -2050 4300
F 0 "#GND?" H -2000 4250 45  0001 L BNN
F 1 "GND" H -2050 4130 45  0000 C CNN
F 2 "" H -2050 4200 60  0001 C CNN
F 3 "" H -2050 4200 60  0001 C CNN
	1    -2050 4300
	0    1    1    0   
$EndComp
Text Notes -2200 4650 0    50   ~ 0
When logic high, outputs are disabled.\nInputs (STEP, DIR, MS1, MS2) are still\naccessible.
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EBC4092
P -2150 2900
F 0 "#SUPPLY?" H -2100 2900 45  0001 L BNN
F 1 "VCC" H -2150 3070 45  0000 C CNN
F 2 "" H -2150 3081 60  0000 C CNN
F 3 "" H -2150 2900 60  0001 C CNN
	1    -2150 2900
	1    0    0    -1  
$EndComp
Text GLabel -1450 3200 2    47   Output ~ 0
SLEEP
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5F415069
P -1650 3200
F 0 "R?" H -1650 3500 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1650 3416 45  0000 C CNN
F 2 "AXIAL-0.3" H -1650 3350 20  0001 C CNN
F 3 "" H -1650 3200 60  0001 C CNN
F 4 "RES-12183" H -1650 3321 60  0000 C CNN "Field4"
	1    -1650 3200
	1    0    0    -1  
$EndComp
Text Notes -3800 1650 0    50   ~ 0
If V at PFD is greater than 0.6 VCC,\nslow decay mode is selected.\nAllows the motor is stop quickly.\n\nIf V at PFD is less than 0.21 VCC,\nfast decay mode is selected.\nResponds more quickly to changing \nstep inputs but stops slowly.\n\nElse, mixed decay mode is selected.\nAllows for microstepping to use the\nbest of both worlds. Mixed mode is\nwhat is selected here (0.4 VCC)
Text Notes -1950 750  0    50   ~ 0
Active Low sets the translator to \npredefined home state:\n(45 Step Angle, DIR = H)\nSTEP inputs are ignored untill \nRESET goes high.
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5F415067
P -2050 650
F 0 "#GND?" H -2000 600 45  0001 L BNN
F 1 "GND" H -2050 480 45  0000 C CNN
F 2 "" H -2050 550 60  0001 C CNN
F 3 "" H -2050 550 60  0001 C CNN
	1    -2050 650 
	1    0    0    -1  
$EndComp
$Comp
L dk_Pushbutton-Switches:GPTS203211B S?
U 1 1 5EB27173
P -2050 250
F 0 "S?" V -2004 206 50  0000 R CNN
F 1 "GPTS203211B" V -2095 206 50  0000 R CNN
F 2 "digikey-footprints:PushButton_12x12mm_THT_GPTS203211B" H -1850 450 50  0001 L CNN
F 3 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H -1850 550 60  0001 L CNN
F 4 "CW181-ND" H -1850 650 60  0001 L CNN "Digi-Key_PN"
F 5 "GPTS203211B" H -1850 750 60  0001 L CNN "MPN"
F 6 "Switches" H -1850 850 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H -1850 950 60  0001 L CNN "Family"
F 8 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H -1850 1050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cw-industries/GPTS203211B/CW181-ND/3190590" H -1850 1150 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSHBUTTON SPST 1A 30V" H -1850 1250 60  0001 L CNN "Description"
F 11 "CW Industries" H -1850 1350 60  0001 L CNN "Manufacturer"
F 12 "Active" H -1850 1450 60  0001 L CNN "Status"
	1    -2050 250 
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EB181CE
P -2050 -200
F 0 "#SUPPLY?" H -2000 -200 45  0001 L BNN
F 1 "VCC" H -2050 -30 45  0000 C CNN
F 2 "" H -2050 -19 60  0000 C CNN
F 3 "" H -2050 -200 60  0001 C CNN
	1    -2050 -200
	1    0    0    -1  
$EndComp
Text GLabel -4900 2600 2    47   Output ~ 0
OUT2B
Text GLabel -4900 2500 2    47   Output ~ 0
OUT2A
Text GLabel -1250 -50  2    47   Output ~ 0
RESET
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB982DD
P -1450 -50
F 0 "R?" H -1450 250 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H -1450 166 45  0000 C CNN
F 2 "AXIAL-0.3" H -1450 100 20  0001 C CNN
F 3 "" H -1450 -50 60  0001 C CNN
F 4 "RES-12183" H -1450 71  60  0000 C CNN "Field4"
	1    -1450 -50 
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5ECC1D70
P -2000 2300
F 0 "#GND?" H -1950 2250 45  0001 L BNN
F 1 "GND" H -2000 2130 45  0000 C CNN
F 2 "" H -2000 2200 60  0001 C CNN
F 3 "" H -2000 2200 60  0001 C CNN
	1    -2000 2300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:POTENTIOMETER-PTH-9MM-1_20W-20% VR?
U 1 1 5F415078
P -2000 1600
F 0 "VR?" H -2068 1505 45  0000 R CNN
F 1 "POTENTIOMETER-PTH-9MM-1_20W-20%" H -2068 1589 45  0000 R CNN
F 2 "POT-PTH-ALPS" V -2150 1600 20  0001 C CNN
F 3 "" H -2000 1600 60  0001 C CNN
F 4 "RES-09177" H -2068 1684 60  0000 R CNN "Field4"
	1    -2000 1600
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Resistors:2.49KOHM-0603-1_10W-1% R?
U 1 1 5F415077
P -2000 2100
F 0 "R?" V -2095 2168 45  0000 L CNN
F 1 "2.49KOHM-0603-1_10W-1%" V -2011 2168 45  0000 L CNN
F 2 "0603" H -2000 2250 20  0001 C CNN
F 3 "" H -2000 2100 60  0001 C CNN
F 4 "RES-09568" V -1916 2168 60  0000 L CNN "Field4"
	1    -2000 2100
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EC6CFA3
P -2000 1300
F 0 "#SUPPLY?" H -1950 1300 45  0001 L BNN
F 1 "VCC" H -2000 1470 45  0000 C CNN
F 2 "" H -2000 1481 60  0000 C CNN
F 3 "" H -2000 1300 60  0001 C CNN
	1    -2000 1300
	1    0    0    -1  
$EndComp
Text Notes -1900 1400 0    47   ~ 0
At REF of 5V -> 833mA/phase\nAt REF of 2V -> 333mA/phase\n\nMinimum current = smootest steps\nMaximum current = highest torque
Text GLabel -2200 1550 1    47   Output ~ 0
REF
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F415080
P -5550 1300
F 0 "#SUPPLY?" H -5500 1300 45  0001 L BNN
F 1 "VOUT" H -5550 1470 45  0000 C CNN
F 2 "" H -5550 1481 60  0000 C CNN
F 3 "" H -5550 1300 60  0001 C CNN
	1    -5550 1300
	0    -1   -1   0   
$EndComp
Text Notes -6250 650  0    50   ~ 0
+- 750mA, 30V output rating\nMin 4.75V load voltage\n3.0 - 5.5 V logic supply\nCrossover protection\nMixed, fast, & slow decay modes\n
Text Notes -6250 100  0    50   ~ 0
Allegro A3967 Microstepping Driver with translator\n\nSimilar to what EasyDriver uses. Nifty little IC that\ntakes care of almost all the logic of it. 
Connection ~ -5550 1300
Connection ~ -5500 1800
Connection ~ -5500 3400
Connection ~ -5400 1800
Connection ~ -5400 3400
Connection ~ -5300 1700
Connection ~ -5300 1800
Connection ~ -5300 3400
Connection ~ -5250 1300
Connection ~ -5200 3400
Connection ~ -4000 3450
Connection ~ -3900 -200
Connection ~ -3900 600 
Connection ~ -3900 4100
Connection ~ -3900 4500
Connection ~ -3650 2050
Connection ~ -3650 2900
Connection ~ -3600 2500
Connection ~ -3600 3350
Connection ~ -3550 4100
Connection ~ -3550 4500
Connection ~ -3300 2500
Connection ~ -3300 3350
Connection ~ -3250 2050
Connection ~ -3250 2900
Connection ~ -3150 4100
Connection ~ -3150 4500
Connection ~ -2200 1600
Connection ~ -2150 2900
Connection ~ -2050 -200
Connection ~ -2050 50  
Connection ~ -2050 450 
Connection ~ -2050 650 
Connection ~ -2050 4300
Connection ~ -2000 1300
Connection ~ -2000 1900
Connection ~ -2000 2300
Connection ~ -1850 3200
Connection ~ -1850 3600
Connection ~ -1850 4000
Connection ~ -1650 -50 
$EndSCHEMATC
