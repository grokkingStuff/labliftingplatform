EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes -2800 5950 0    50   ~ 0
Inter-Integrated Circuit (I2C)\n\nSDA: Serial Data, used to send and receive data\nSCL: Serial Clock, used to carry clock signal\n\nSynchronous protocol with only two wires. \nUsed quite a bit to interface with sensors.\n\nUnfortunately, two conductive wires that extend for some \ndistance acts as a "bus capacitor" that  degrades signal \nquality. Using stronger pullup resistors can charge that \nparasitic capacitor quicker but also means the higher current \nthat flows through your fragile sensors, which places a hard \nlimit on such finesses. Using a differential pair helps reduce \nthe parastic losses and is more able to handle EMI.
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E98AA3E
P 10600 15800
F 0 "#GND?" H 10650 15750 45  0001 L BNN
F 1 "GND" H 10600 15630 45  0000 C CNN
F 2 "" H 10600 15700 60  0001 C CNN
F 3 "" H 10600 15700 60  0001 C CNN
	1    10600 15800
	1    0    0    -1  
$EndComp
Text GLabel 12650 15450 0    50   Input ~ 0
DSCL0_N
Text GLabel 12650 15050 0    50   Input ~ 0
DSCL0_P
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E992798
P 12800 15850
F 0 "#GND?" H 12850 15800 45  0001 L BNN
F 1 "GND" H 12800 15680 45  0000 C CNN
F 2 "" H 12800 15750 60  0001 C CNN
F 3 "" H 12800 15750 60  0001 C CNN
	1    12800 15850
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E99279F
P 12800 14850
F 0 "R?" V 12705 14918 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12789 14918 45  0000 L CNN
F 2 "0603" H 12800 15000 20  0001 C CNN
F 3 "" H 12800 14850 60  0001 C CNN
F 4 "RES-07857" V 12884 14918 60  0000 L CNN "Field4"
	1    12800 14850
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E9927A6
P 12800 15250
F 0 "R?" V 12705 15318 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12789 15318 45  0000 L CNN
F 2 "0603" H 12800 15400 20  0001 C CNN
F 3 "" H 12800 15250 60  0001 C CNN
F 4 "RES-07857" V 12884 15318 60  0000 L CNN "Field4"
	1    12800 15250
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E9927AD
P 12800 15650
F 0 "R?" V 12705 15718 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 12789 15718 45  0000 L CNN
F 2 "0603" H 12800 15800 20  0001 C CNN
F 3 "" H 12800 15650 60  0001 C CNN
F 4 "RES-07857" V 12884 15718 60  0000 L CNN "Field4"
	1    12800 15650
	0    1    1    0   
$EndComp
Wire Wire Line
	12650 15050 12800 15050
Connection ~ 12800 15050
Wire Wire Line
	12650 15450 12800 15450
Connection ~ 12800 15450
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E9959EE
P 12800 14650
F 0 "#SUPPLY?" H 12850 14650 45  0001 L BNN
F 1 "3.3V" H 12800 14820 45  0000 C CNN
F 2 "" H 12800 14831 60  0000 C CNN
F 3 "" H 12800 14650 60  0001 C CNN
	1    12800 14650
	1    0    0    -1  
$EndComp
Text GLabel 12950 15050 2    50   Output ~ 0
DSCLP
Text GLabel 12950 15450 2    50   Output ~ 0
DSCLM
Wire Wire Line
	12950 15050 12800 15050
Wire Wire Line
	12950 15450 12800 15450
Text GLabel -3300 6400 2    50   Input ~ 0
DSDAP
Text GLabel -3300 6500 2    50   Input ~ 0
DSDAM
Text GLabel -3300 6300 2    50   Input ~ 0
DSCLP
Text GLabel -3300 6200 2    50   Input ~ 0
DSCLM
Text GLabel -3300 6600 2    50   Output ~ 0
SCL
Text GLabel -3300 6700 2    50   Output ~ 0
SDA
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5E9A3AAE
P -4400 8350
F 0 "C?" V -4060 8400 45  0000 C CNN
F 1 "0.1UF-0603-100V-10%" V -4144 8400 45  0000 C CNN
F 2 "0603" H -4400 8600 20  0001 C CNN
F 3 "" H -4400 8350 50  0001 C CNN
F 4 "CAP-08390" V -4239 8400 60  0000 C CNN "Field4"
	1    -4400 8350
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-Capacitors:100UF-POLAR-10X10.5-63V-20% C?
U 1 1 5E9A70A6
P -4400 7850
F 0 "C?" V -4760 7800 45  0000 C CNN
F 1 "100UF-POLAR-10X10.5-63V-20%" V -4676 7800 45  0000 C CNN
F 2 "NIC_10X10.5_CAP" H -4400 8100 20  0001 C CNN
F 3 "" H -4400 7850 50  0001 C CNN
F 4 "CAP-08362" V -4581 7800 60  0000 C CNN "Field4"
	1    -4400 7850
	0    1    1    0   
$EndComp
Wire Wire Line
	10750 15400 10600 15400
Wire Wire Line
	10750 15000 10600 15000
Text GLabel 10750 15000 2    50   Output ~ 0
DSDAP
Text GLabel 10750 15400 2    50   Output ~ 0
DSDAM
Connection ~ 10600 15400
Wire Wire Line
	10450 15400 10600 15400
Connection ~ 10600 15000
Wire Wire Line
	10450 15000 10600 15000
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98D8A7
P 10600 15600
F 0 "R?" V 10505 15668 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10589 15668 45  0000 L CNN
F 2 "0603" H 10600 15750 20  0001 C CNN
F 3 "" H 10600 15600 60  0001 C CNN
F 4 "RES-07857" V 10684 15668 60  0000 L CNN "Field4"
	1    10600 15600
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98D37A
P 10600 15200
F 0 "R?" V 10505 15268 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10589 15268 45  0000 L CNN
F 2 "0603" H 10600 15350 20  0001 C CNN
F 3 "" H 10600 15200 60  0001 C CNN
F 4 "RES-07857" V 10684 15268 60  0000 L CNN "Field4"
	1    10600 15200
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5E98C2CF
P 10600 14800
F 0 "R?" V 10505 14868 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 10589 14868 45  0000 L CNN
F 2 "0603" H 10600 14950 20  0001 C CNN
F 3 "" H 10600 14800 60  0001 C CNN
F 4 "RES-07857" V 10684 14868 60  0000 L CNN "Field4"
	1    10600 14800
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E98A2EF
P 10600 14600
F 0 "#SUPPLY?" H 10650 14600 45  0001 L BNN
F 1 "3.3V" H 10600 14770 45  0000 C CNN
F 2 "" H 10600 14781 60  0000 C CNN
F 3 "" H 10600 14600 60  0001 C CNN
	1    10600 14600
	1    0    0    -1  
$EndComp
Text GLabel 10450 15000 0    50   Input ~ 0
DSDA0_P
Text GLabel 10450 15400 0    50   Input ~ 0
DSDA0_N
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C9695
P 2350 8900
F 0 "JP?" H 2454 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 9150 20  0001 C CNN
F 3 "" H 2350 8900 60  0001 C CNN
F 4 "" H 2454 8816 60  0000 L CNN "PROD_ID"
	1    2350 8900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_1-NC JP?
U 1 1 5E9CA69F
P 1850 8900
F 0 "JP?" H 1954 8858 45  0000 L CNN
F 1 "JUMPER-SMT_3_1-NC" H 1954 8942 45  0000 L CNN
F 2 "SMT-JUMPER_3_1-NC" H 1850 9150 20  0001 C CNN
F 3 "" V 1850 8845 60  0001 C CNN
F 4 "" H 1954 8984 60  0000 L CNN "PROD_ID"
	1    1850 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 8050 1850 8200
Wire Wire Line
	2350 8200 2350 8050
Wire Wire Line
	2350 7650 2350 7550
Wire Wire Line
	2350 7550 1850 7550
Wire Wire Line
	1850 7550 1850 7650
Wire Wire Line
	2350 8700 2350 8600
Wire Wire Line
	2350 8600 1850 8600
Wire Wire Line
	1850 8600 1850 8700
Wire Wire Line
	1850 9100 1850 9250
Wire Wire Line
	1850 9250 2350 9250
Wire Wire Line
	2350 9250 2350 9100
$Comp
L dk_PMIC-Current-Regulation-Management:INA219AIDCNR U?
U 1 1 5E9EFC77
P 4050 8100
F 0 "U?" H 4178 8103 60  0000 L CNN
F 1 "INA219AIDCNR" H 4178 7997 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-8" H 4250 8300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8400 60  0001 L CNN
F 4 "296-23770-1-ND" H 4250 8500 60  0001 L CNN "Digi-Key_PN"
F 5 "INA219AIDCNR" H 4250 8600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4250 8700 60  0001 L CNN "Category"
F 7 "PMIC - Current Regulation/Management" H 4250 8800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 4250 8900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/INA219AIDCNR/296-23770-1-ND/1952550" H 4250 9000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC CURRENT MONITOR 1% SOT23-8" H 4250 9100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4250 9200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4250 9300 60  0001 L CNN "Status"
	1    4050 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 8200 1850 8200
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C969C
P 1850 7850
F 0 "JP?" H 1954 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1954 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 1850 8100 20  0001 C CNN
F 3 "" H 1850 7850 60  0001 C CNN
F 4 "" H 1954 7766 60  0000 L CNN "PROD_ID"
	1    1850 7850
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP?
U 1 1 5E9C578F
P 2350 7850
F 0 "JP?" H 2454 7808 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2454 7892 45  0000 L CNN
F 2 "SMT-JUMPER_3_NO" H 2350 8100 20  0001 C CNN
F 3 "" H 2350 7850 60  0001 C CNN
F 4 "" H 2454 7766 60  0000 L CNN "PROD_ID"
	1    2350 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 8900 2200 8900
Wire Wire Line
	2000 8900 2100 8900
Connection ~ 2100 8900
Wire Wire Line
	2100 8400 2100 8900
Wire Wire Line
	2000 7850 2100 7850
Wire Wire Line
	2100 8300 2100 7850
Connection ~ 2100 7850
Wire Wire Line
	2100 7850 2200 7850
Wire Wire Line
	2350 7550 3150 7550
Wire Wire Line
	3150 7550 3150 8100
Connection ~ 2350 7550
Connection ~ 2350 8200
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EA298B3
P 950 7100
F 0 "#SUPPLY?" H 1000 7100 45  0001 L BNN
F 1 "VCC" H 950 7270 45  0000 C CNN
F 2 "" H 950 7281 60  0000 C CNN
F 3 "" H 950 7100 60  0001 C CNN
	1    950  7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  8600 1850 8600
Connection ~ 1850 8600
Wire Wire Line
	4050 7200 4050 7700
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5EA31DD3
P 3750 9000
F 0 "C?" H 3858 9145 45  0000 L CNN
F 1 "0.1UF-0603-100V-10%" H 3858 9061 45  0000 L CNN
F 2 "0603" H 3750 9250 20  0001 C CNN
F 3 "" H 3750 9000 50  0001 C CNN
F 4 "CAP-08390" H 3858 8966 60  0000 L CNN "Field4"
	1    3750 9000
	-1   0    0    1   
$EndComp
Connection ~ 2350 8600
Wire Wire Line
	3750 9200 3750 9250
Wire Wire Line
	3750 9250 4050 9250
Wire Wire Line
	4050 9250 4050 8600
Connection ~ 2350 9250
Connection ~ 3750 9250
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA41E4C
P 950 9350
F 0 "#GND?" H 1000 9300 45  0001 L BNN
F 1 "GND" H 950 9180 45  0000 C CNN
F 2 "" H 950 9250 60  0001 C CNN
F 3 "" H 950 9250 60  0001 C CNN
	1    950  9350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 9250 950  9250
Wire Wire Line
	950  9250 950  9350
Connection ~ 1850 9250
Text Notes 1100 8400 0    50   ~ 0
I2C Address Selection
Wire Wire Line
	3650 7900 3650 7600
Wire Wire Line
	3650 8000 3550 8000
Wire Wire Line
	3550 8000 3550 7500
Wire Wire Line
	3650 7600 4500 7600
Wire Wire Line
	3550 7500 4500 7500
Text GLabel 4500 7600 2    50   Output ~ 0
IN+
Text GLabel 4500 7500 2    50   Output ~ 0
IN-
Wire Wire Line
	3650 8200 3450 8200
Text GLabel 4500 7400 2    50   BiDi ~ 0
SDA
Text GLabel 4500 7300 2    50   BiDi ~ 0
SCL
Wire Wire Line
	4500 7400 3450 7400
Wire Wire Line
	3450 7400 3450 8200
Connection ~ 3450 8200
Wire Wire Line
	4500 7300 3350 7300
Wire Wire Line
	3350 7300 3350 8100
Connection ~ 3350 8100
Wire Wire Line
	3350 8100 3650 8100
Wire Wire Line
	2100 8300 3650 8300
Wire Wire Line
	2100 8400 3650 8400
Wire Wire Line
	3150 8100 3350 8100
Wire Wire Line
	2350 8200 3450 8200
Wire Wire Line
	2350 8600 3750 8600
Wire Wire Line
	2350 9250 3750 9250
Wire Wire Line
	-4700 6850 -4800 6850
Wire Wire Line
	-4800 6850 -4800 6500
Wire Wire Line
	-4700 6950 -4900 6950
Wire Wire Line
	-4900 6950 -4900 6400
Wire Wire Line
	-4700 7050 -5000 7050
Wire Wire Line
	-5000 7050 -5000 6300
Wire Wire Line
	-4700 7150 -5100 7150
Wire Wire Line
	-5100 7150 -5100 6200
Wire Wire Line
	-3700 6950 -3600 6950
Wire Wire Line
	-3600 6950 -3600 6600
Wire Wire Line
	-3700 7150 -3500 7150
Wire Wire Line
	-3500 7150 -3500 6700
Wire Wire Line
	-3700 7050 -3400 7050
Wire Wire Line
	-3700 6850 -3400 6850
Wire Wire Line
	-4700 7250 -5200 7250
Connection ~ -5200 7250
$Comp
L PCA9615:PCA9615 U?
U 1 1 5E99CC70
P -4200 7050
F 0 "U?" H -4200 6613 60  0000 C CNN
F 1 "PCA9615" H -4200 6719 60  0000 C CNN
F 2 "" H -4200 7050 60  0001 C CNN
F 3 "" H -4200 7050 60  0001 C CNN
	1    -4200 7050
	-1   0    0    1   
$EndComp
Wire Wire Line
	-3700 7250 -3600 7250
Wire Wire Line
	-5200 7400 -3600 7400
Wire Wire Line
	-3600 7250 -3600 7400
Wire Wire Line
	-3600 7400 -3600 7850
Wire Wire Line
	-3600 7850 -4300 7850
Connection ~ -3600 7400
Wire Wire Line
	-3600 7850 -3600 8350
Wire Wire Line
	-3600 8350 -4300 8350
Connection ~ -3600 7850
Wire Wire Line
	-5200 8350 -4600 8350
Connection ~ -3400 7050
Wire Wire Line
	-5200 7850 -5200 8350
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EB288F7
P -5200 6200
F 0 "#SUPPLY?" H -5150 6200 45  0001 L BNN
F 1 "VCC" H -5200 6370 45  0000 C CNN
F 2 "" H -5200 6381 60  0000 C CNN
F 3 "" H -5200 6200 60  0001 C CNN
	1    -5200 6200
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EB28D07
P -5200 9100
F 0 "#GND?" H -5150 9050 45  0001 L BNN
F 1 "GND" H -5200 8930 45  0000 C CNN
F 2 "" H -5200 9000 60  0001 C CNN
F 3 "" H -5200 9000 60  0001 C CNN
	1    -5200 9100
	1    0    0    -1  
$EndComp
Text Notes -2800 4650 0    157  Italic 0
Differential I2C
Text Notes 750  6600 0    157  Italic 0
Power Monitor
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB91C0D
P 6550 4650
F 0 "R?" H 6550 4950 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6550 4866 45  0000 C CNN
F 2 "AXIAL-0.3" H 6550 4800 20  0001 C CNN
F 3 "" H 6550 4650 60  0001 C CNN
F 4 "RES-12183" H 6550 4771 60  0000 C CNN "Field4"
	1    6550 4650
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB950B0
P 6550 5050
F 0 "R?" H 6550 5350 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6550 5266 45  0000 C CNN
F 2 "AXIAL-0.3" H 6550 5200 20  0001 C CNN
F 3 "" H 6550 5050 60  0001 C CNN
F 4 "RES-12183" H 6550 5171 60  0000 C CNN "Field4"
	1    6550 5050
	1    0    0    -1  
$EndComp
Text GLabel 6750 4650 2    47   Output ~ 0
MS1
Text GLabel 6750 5050 2    47   Output ~ 0
MS2
Wire Wire Line
	6050 4650 6050 5050
Wire Wire Line
	6050 4250 6050 4650
Text GLabel 5450 5150 2    47   Output ~ 0
SENSE1
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EBDB29D
P 4850 5150
F 0 "R?" H 4850 5450 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H 4850 5366 45  0000 C CNN
F 2 "0805" H 4850 5300 20  0001 C CNN
F 3 "" H 4850 5150 60  0001 C CNN
F 4 "RES-08474" H 4850 5271 60  0000 C CNN "Field4"
	1    4850 5150
	1    0    0    -1  
$EndComp
Text GLabel 5350 3950 2    47   Output ~ 0
RC1
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EBEB9C4
P 4750 3950
F 0 "R?" H 4750 4144 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H 4750 4060 45  0000 C CNN
F 2 "0603" H 4750 4100 20  0001 C CNN
F 3 "" H 4750 3950 60  0001 C CNN
	1    4750 3950
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EBED03C
P 4700 4400
F 0 "C?" V 4360 4450 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V 4444 4450 45  0000 C CNN
F 2 "0603" H 4700 4650 20  0001 C CNN
F 3 "" H 4700 4400 50  0001 C CNN
F 4 "CAP-09232" V 4539 4450 60  0000 C CNN "Field4"
	1    4700 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3950 4200 3950
Wire Wire Line
	4200 4400 4600 4400
Wire Wire Line
	4950 3950 5250 3950
Wire Wire Line
	5250 3950 5250 4400
Wire Wire Line
	5250 4400 4900 4400
Wire Wire Line
	4300 5150 4650 5150
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC2F662
P 4200 4500
F 0 "#GND?" H 4250 4450 45  0001 L BNN
F 1 "GND" H 4200 4330 45  0000 C CNN
F 2 "" H 4200 4400 60  0001 C CNN
F 3 "" H 4200 4400 60  0001 C CNN
	1    4200 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3950 5250 3950
Connection ~ 5250 3950
Wire Wire Line
	5050 5150 5450 5150
Text GLabel 5450 5550 2    47   Output ~ 0
SENSE2
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EC4F9D1
P 4850 5550
F 0 "R?" H 4850 5850 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H 4850 5766 45  0000 C CNN
F 2 "0805" H 4850 5700 20  0001 C CNN
F 3 "" H 4850 5550 60  0001 C CNN
F 4 "RES-08474" H 4850 5671 60  0000 C CNN "Field4"
	1    4850 5550
	1    0    0    -1  
$EndComp
Text GLabel 5350 3100 2    47   Output ~ 0
RC2
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EC4F9D8
P 4750 3100
F 0 "R?" H 4750 3294 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H 4750 3210 45  0000 C CNN
F 2 "0603" H 4750 3250 20  0001 C CNN
F 3 "" H 4750 3100 60  0001 C CNN
	1    4750 3100
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EC4F9DF
P 4700 3550
F 0 "C?" V 4360 3600 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V 4444 3600 45  0000 C CNN
F 2 "0603" H 4700 3800 20  0001 C CNN
F 3 "" H 4700 3550 50  0001 C CNN
F 4 "CAP-09232" V 4539 3600 60  0000 C CNN "Field4"
	1    4700 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3100 4200 3100
Wire Wire Line
	4200 3550 4600 3550
Wire Wire Line
	4950 3100 5250 3100
Wire Wire Line
	5250 3100 5250 3550
Wire Wire Line
	5250 3550 4900 3550
Wire Wire Line
	4300 5550 4650 5550
Wire Wire Line
	5350 3100 5250 3100
Connection ~ 5250 3100
Wire Wire Line
	5050 5550 5450 5550
Text GLabel 7300 5350 2    47   Output ~ 0
ENABLE
Connection ~ 4200 4400
Connection ~ 4200 3550
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5ED13BA2
P 4300 1650
F 0 "#GND?" H 4350 1600 45  0001 L BNN
F 1 "GND" H 4300 1480 45  0000 C CNN
F 2 "" H 4300 1550 60  0001 C CNN
F 3 "" H 4300 1550 60  0001 C CNN
	1    4300 1650
	-1   0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5ED14FB6
P 4300 1450
F 0 "R?" V 4205 1518 45  0000 L CNN
F 1 "10KOHM-0603-1_10W-1%" V 4289 1518 45  0000 L CNN
F 2 "0603" H 4300 1600 20  0001 C CNN
F 3 "" H 4300 1450 60  0001 C CNN
F 4 "RES-00824" V 4384 1518 60  0000 L CNN "Field4"
	1    4300 1450
	0    1    -1   0   
$EndComp
$Comp
L SparkFun-Resistors:6.8KOHM-0603-1_10W-1% R?
U 1 1 5ED162DB
P 4300 1050
F 0 "R?" V 4395 982 45  0000 R CNN
F 1 "6.8KOHM-0603-1_10W-1%" V 4311 982 45  0000 R CNN
F 2 "0603" H 4300 1200 20  0001 C CNN
F 3 "" H 4300 1050 60  0001 C CNN
F 4 "RES-08597" V 4216 982 60  0000 R CNN "Field4"
	1    4300 1050
	0    -1   1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EE2A63F
P 4300 850
F 0 "#SUPPLY?" H 4350 850 45  0001 L BNN
F 1 "VCC" H 4300 1020 45  0000 C CNN
F 2 "" H 4300 1031 60  0000 C CNN
F 3 "" H 4300 850 60  0001 C CNN
	1    4300 850 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 8600 3750 8900
Wire Wire Line
	-3300 6700 -3500 6700
Wire Wire Line
	-3600 6600 -3300 6600
Wire Wire Line
	-3300 6500 -4800 6500
Wire Wire Line
	-4900 6400 -3300 6400
Wire Wire Line
	-3300 6300 -5000 6300
Wire Wire Line
	-5100 6200 -3300 6200
Wire Wire Line
	-5200 7250 -5200 6200
Wire Wire Line
	-5200 7250 -5200 7400
Wire Wire Line
	-3400 6850 -3400 7050
Wire Wire Line
	-5200 7850 -4600 7850
$Comp
L SparkFun-LED:LED-BLUE0603 D?
U 1 1 5F0F44C8
P -4750 8800
F 0 "D?" V -5145 8750 45  0000 C CNN
F 1 "LED-BLUE0603" V -5061 8750 45  0000 C CNN
F 2 "LED-0603" V -4450 8800 20  0001 C CNN
F 3 "" H -4750 8800 50  0001 C CNN
F 4 "DIO-08575" V -4966 8750 60  0000 C CNN "Field4"
	1    -4750 8800
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5F0F6136
P -4000 8800
F 0 "R?" H -4000 9100 45  0000 C CNN
F 1 "10KOHM-0603-1_10W-1%" H -4000 9016 45  0000 C CNN
F 2 "0603" H -4000 8950 20  0001 C CNN
F 3 "" H -4000 8800 60  0001 C CNN
F 4 "RES-00824" H -4000 8921 60  0000 C CNN "Field4"
	1    -4000 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	-4200 8800 -4650 8800
Wire Wire Line
	-3400 8800 -3800 8800
Wire Wire Line
	-3400 7050 -3400 8800
Wire Wire Line
	-4950 8800 -5200 8800
Wire Wire Line
	-5200 8800 -5200 9100
Wire Wire Line
	-5200 8350 -5200 8800
Connection ~ -5200 8350
Connection ~ -5200 8800
Text GLabel 10000 14650 2    50   Output ~ 0
DSDA0_N
Text GLabel 10000 14550 2    50   Output ~ 0
DSDA0_P
Text GLabel 10000 14350 2    50   Output ~ 0
DSCL0_P
Text GLabel 10000 14450 2    50   Output ~ 0
DSCL0_N
$Comp
L SparkFun-Connectors:CONN_04SCREW J?
U 1 1 5F161A9A
P 9900 14650
F 0 "J?" H 9858 15260 45  0000 C CNN
F 1 "CONN_04SCREW" H 9858 15176 45  0000 C CNN
F 2 "SCREWTERMINAL-3.5MM-4" H 9900 15150 20  0001 C CNN
F 3 "" H 9900 14650 50  0001 C CNN
F 4 "2xCONN-08399" H 9858 15081 60  0000 C CNN "Field4"
	1    9900 14650
	1    0    0    -1  
$EndComp
Text GLabel 4350 1250 2    48   Input ~ 0
PFD
Wire Wire Line
	4350 1250 4300 1250
Connection ~ 4300 1250
$Comp
L SparkFun-IC-Microcontroller:ATMEGA32U4 U?
U 1 1 5F20C992
P -4000 2450
F 0 "U?" H -3532 4260 45  0000 C CNN
F 1 "ATMEGA32U4" H -3532 4176 45  0000 C CNN
F 2 "QFN-44" H -4000 4100 20  0001 C CNN
F 3 "" H -4000 2450 50  0001 C CNN
F 4 "IC-10828" H -3532 4081 60  0000 C CNN "Field4"
	1    -4000 2450
	1    0    0    -1  
$EndComp
Text GLabel -3300 2200 2    50   Input ~ 0
SDA
Text GLabel -3300 2100 2    50   Input ~ 0
SCL
Text GLabel -4700 1000 0    47   Input ~ 0
RESET
Wire Wire Line
	3250 7200 3250 7450
Wire Wire Line
	3250 7450 950  7450
Wire Wire Line
	3250 7200 4050 7200
Connection ~ 950  7450
Wire Wire Line
	950  7450 950  8600
Wire Wire Line
	950  7100 950  7450
Text Notes 800  6800 0    47   ~ 0
Monitor the voltage and current and sends back\ninformation to master. Useful for logs
Text Notes 600  800  0    157  Italic 0
Motor Driver\n
Text GLabel 1550 3350 0    47   Output ~ 0
REF
$Comp
L dk_PMIC-Motor-Drivers-Controllers:A3967SLBTR-T U?
U 1 1 5EB6D200
P 2050 3950
F 0 "U?" H 2250 3700 60  0000 C CNN
F 1 "A3967SLBTR-T" V 2550 3500 60  0000 C CNN
F 2 "digikey-footprints:SOIC-24_W7.50mm" H 2250 4150 60  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H 2250 4250 60  0001 L CNN
F 4 "620-1140-1-ND" H 2250 4350 60  0001 L CNN "Digi-Key_PN"
F 5 "A3967SLBTR-T" H 2250 4450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2250 4550 60  0001 L CNN "Category"
F 7 "PMIC - Motor Drivers, Controllers" H 2250 4650 60  0001 L CNN "Family"
F 8 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H 2250 4750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/allegro-microsystems-llc/A3967SLBTR-T/620-1140-1-ND/1090383" H 2250 4850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MTR DRV BIPOLAR 3-5.5V 24SOIC" H 2250 4950 60  0001 L CNN "Description"
F 11 "Allegro MicroSystems, LLC" H 2250 5050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2250 5150 60  0001 L CNN "Status"
	1    2050 3950
	1    0    0    -1  
$EndComp
Text GLabel 2550 3650 2    47   Output ~ 0
OUT1A
Text GLabel 2550 3750 2    47   Output ~ 0
OUT1B
Text GLabel 1550 3550 0    47   Input ~ 0
SLEEP
Text GLabel 1550 4050 0    47   Input ~ 0
MS2
Text GLabel 1550 4350 0    47   Input ~ 0
RESET
Text GLabel 1550 3450 0    47   Input ~ 0
RC2
Text GLabel 1550 4450 0    47   Input ~ 0
RC1
Text GLabel 1550 4250 0    47   Input ~ 0
SENSE1
Text GLabel 1550 3650 0    47   Input ~ 0
SENSE2
Text GLabel 1550 3950 0    47   Input ~ 0
MS1
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC39CD6
P 1950 4850
F 0 "#GND?" H 2000 4800 45  0001 L BNN
F 1 "GND" H 1950 4680 45  0000 C CNN
F 2 "" H 1950 4750 60  0001 C CNN
F 3 "" H 1950 4750 60  0001 C CNN
	1    1950 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4750 2250 4850
Wire Wire Line
	2250 4850 2150 4850
Wire Wire Line
	1950 4750 1950 4850
Connection ~ 1950 4850
Wire Wire Line
	2050 4750 2050 4850
Connection ~ 2050 4850
Wire Wire Line
	2050 4850 1950 4850
Wire Wire Line
	2150 4750 2150 4850
Connection ~ 2150 4850
Wire Wire Line
	2150 4850 2050 4850
Text GLabel 1550 3750 0    47   Input ~ 0
STEP
Text GLabel 1550 3850 0    47   Input ~ 0
DIR
Text GLabel 1550 4150 0    47   Input ~ 0
ENABLE
Text GLabel 1550 4550 0    48   Input ~ 0
PFD
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5F56C56A
P 2150 3050
F 0 "#SUPPLY?" H 2200 3050 45  0001 L BNN
F 1 "VCC" H 2150 3220 45  0000 C CNN
F 2 "" H 2150 3231 60  0000 C CNN
F 3 "" H 2150 3050 60  0001 C CNN
	1    2150 3050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F56D965
P 1950 3050
F 0 "#SUPPLY?" H 2000 3050 45  0001 L BNN
F 1 "VOUT" H 1950 3220 45  0000 C CNN
F 2 "" H 1950 3231 60  0000 C CNN
F 3 "" H 1950 3050 60  0001 C CNN
	1    1950 3050
	1    0    0    -1  
$EndComp
$Comp
L dk_PMIC-Voltage-Regulators-Linear:MCP1700-3302E_TO U?
U 1 1 5E9A491C
P 12000 16300
F 0 "U?" H 12000 16587 60  0000 C CNN
F 1 "MCP1700-3302E_TO" H 12000 16481 60  0000 C CNN
F 2 "digikey-footprints:TO-92-3" H 12200 16500 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 12200 16600 60  0001 L CNN
F 4 "MCP1700-3302E/TO-ND" H 12200 16700 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP1700-3302E/TO" H 12200 16800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 12200 16900 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 12200 17000 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 12200 17100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP1700-3302E-TO/MCP1700-3302E-TO-ND/652680" H 12200 17200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 250MA TO92-3" H 12200 17300 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 12200 17400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 12200 17500 60  0001 L CNN "Status"
	1    12000 16300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9BFD41
P 12000 16600
F 0 "#GND?" H 12050 16550 45  0001 L BNN
F 1 "GND" H 12000 16430 45  0000 C CNN
F 2 "" H 12000 16500 60  0001 C CNN
F 3 "" H 12000 16500 60  0001 C CNN
	1    12000 16600
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5E9C0E02
P 12400 16500
F 0 "C?" H 12292 16455 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 12292 16539 45  0000 R CNN
F 2 "0603" H 12400 16750 20  0001 C CNN
F 3 "" H 12400 16500 50  0001 C CNN
F 4 "CAP-00868" H 12292 16634 60  0000 R CNN "Field4"
	1    12400 16500
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Capacitors:1.0UF-0603-16V-10% C?
U 1 1 5E9C596F
P 11450 16500
F 0 "C?" H 11343 16455 45  0000 R CNN
F 1 "1.0UF-0603-16V-10%" H 11343 16539 45  0000 R CNN
F 2 "0603" H 11450 16750 20  0001 C CNN
F 3 "" H 11450 16500 50  0001 C CNN
F 4 "CAP-00868" H 11343 16634 60  0000 R CNN "Field4"
	1    11450 16500
	1    0    0    1   
$EndComp
Wire Wire Line
	12400 16400 12400 16300
Wire Wire Line
	12400 16300 12300 16300
Wire Wire Line
	11700 16300 11450 16300
Wire Wire Line
	11450 16300 11450 16400
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9D5E7E
P 11450 16700
F 0 "#GND?" H 11500 16650 45  0001 L BNN
F 1 "GND" H 11450 16530 45  0000 C CNN
F 2 "" H 11450 16600 60  0001 C CNN
F 3 "" H 11450 16600 60  0001 C CNN
	1    11450 16700
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5E9DCFA8
P 12400 16700
F 0 "#GND?" H 12450 16650 45  0001 L BNN
F 1 "GND" H 12400 16530 45  0000 C CNN
F 2 "" H 12400 16600 60  0001 C CNN
F 3 "" H 12400 16600 60  0001 C CNN
	1    12400 16700
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5E9E4534
P 11450 16150
F 0 "#SUPPLY?" H 11500 16150 45  0001 L BNN
F 1 "3.3V" H 11450 16320 45  0000 C CNN
F 2 "" H 11450 16331 60  0000 C CNN
F 3 "" H 11450 16150 60  0001 C CNN
	1    11450 16150
	1    0    0    -1  
$EndComp
Wire Wire Line
	11450 16150 11450 16300
Connection ~ 11450 16300
$Comp
L SparkFun-Capacitors:47UF-POLAR-EIA3528-10V-10% C?
U 1 1 5EA1B43D
P 2000 2650
F 0 "C?" V 2360 2600 45  0000 C CNN
F 1 "47UF-POLAR-EIA3528-10V-10%" V 2276 2600 45  0000 C CNN
F 2 "EIA3528" H 2000 2900 20  0001 C CNN
F 3 "" H 2000 2650 50  0001 C CNN
F 4 "CAP-08310" V 2181 2600 60  0000 C CNN "Field4"
	1    2000 2650
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA3D799
P 2200 2650
F 0 "#GND?" H 2250 2600 45  0001 L BNN
F 1 "GND" H 2200 2480 45  0000 C CNN
F 2 "" H 2200 2550 60  0001 C CNN
F 3 "" H 2200 2550 60  0001 C CNN
	1    2200 2650
	0    -1   -1   0   
$EndComp
Text Notes 1350 5400 0    50   ~ 0
Star ground system located close \nto driver is recommended. The analog\n ground and power ground are \ninternally bonded. (PINS 6,7,18,19)
Connection ~ 4200 3950
Wire Wire Line
	4200 3950 4200 4400
Wire Wire Line
	4200 4400 4200 4500
Wire Wire Line
	4200 3100 4200 3550
Text Notes 4150 6000 0    50   ~ 0
To minimize voltage drop from current \nsensing to ground, use a low value \nresistor, connect to ground with individual \npaths as close as possible. 
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA75FCF
P 4300 5150
F 0 "#GND?" H 4350 5100 45  0001 L BNN
F 1 "GND" H 4300 4980 45  0000 C CNN
F 2 "" H 4300 5050 60  0001 C CNN
F 3 "" H 4300 5050 60  0001 C CNN
	1    4300 5150
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EA7D426
P 4300 5550
F 0 "#GND?" H 4350 5500 45  0001 L BNN
F 1 "GND" H 4300 5380 45  0000 C CNN
F 2 "" H 4300 5450 60  0001 C CNN
F 3 "" H 4300 5450 60  0001 C CNN
	1    4300 5550
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 3550 4200 3950
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EAA0AC9
P 6150 5350
F 0 "#GND?" H 6200 5300 45  0001 L BNN
F 1 "GND" H 6150 5180 45  0000 C CNN
F 2 "" H 6150 5250 60  0001 C CNN
F 3 "" H 6150 5250 60  0001 C CNN
	1    6150 5350
	0    1    1    0   
$EndComp
Text Notes 6000 5700 0    50   ~ 0
When logic high, outputs are disabled.\nInputs (STEP, DIR, MS1, MS2) are still\naccessible.
Wire Wire Line
	6150 5350 7300 5350
Connection ~ 6050 4650
Wire Wire Line
	6050 3950 6050 4250
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EBC4092
P 6050 3950
F 0 "#SUPPLY?" H 6100 3950 45  0001 L BNN
F 1 "VCC" H 6050 4120 45  0000 C CNN
F 2 "" H 6050 4131 60  0000 C CNN
F 3 "" H 6050 3950 60  0001 C CNN
	1    6050 3950
	1    0    0    -1  
$EndComp
Text GLabel 6750 4250 2    47   Output ~ 0
SLEEP
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB9178A
P 6550 4250
F 0 "R?" H 6550 4550 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6550 4466 45  0000 C CNN
F 2 "AXIAL-0.3" H 6550 4400 20  0001 C CNN
F 3 "" H 6550 4250 60  0001 C CNN
F 4 "RES-12183" H 6550 4371 60  0000 C CNN "Field4"
	1    6550 4250
	1    0    0    -1  
$EndComp
Connection ~ 6050 4250
Wire Wire Line
	10100 15850 10100 16200
Wire Wire Line
	9800 16900 9800 16800
Wire Wire Line
	9700 16800 9700 16900
Wire Wire Line
	9700 16800 9800 16800
Wire Wire Line
	9900 15850 9900 16100
Text Notes 4400 2700 0    50   ~ 0
If V at PFD is greater than 0.6 VCC,\nslow decay mode is selected.\nAllows the motor is stop quickly.\n\nIf V at PFD is less than 0.21 VCC,\nfast decay mode is selected.\nResponds more quickly to changing \nstep inputs but stops slowly.\n\nElse, mixed decay mode is selected.\nAllows for microstepping to use the\nbest of both worlds. Mixed mode is\nwhat is selected here (0.4 VCC)
Wire Notes Line
	4050 4800 4050 6050
Wire Notes Line
	5850 6050 5850 4800
Wire Notes Line
	4050 4800 5850 4800
Wire Notes Line
	4050 6050 5850 6050
Wire Notes Line
	5900 5200 5900 5750
Wire Notes Line
	7700 5750 7700 5200
Wire Notes Line
	5900 5200 7700 5200
Wire Notes Line
	5900 5150 5900 3650
Wire Notes Line
	5900 5150 7700 5150
Wire Notes Line
	5900 3650 7700 3650
Wire Notes Line
	7700 3650 7700 5150
Wire Wire Line
	6350 4250 6050 4250
Wire Wire Line
	6350 4650 6050 4650
Wire Wire Line
	6350 5050 6050 5050
Wire Notes Line
	5900 600  7700 600 
Wire Notes Line
	7700 1950 7700 600 
Wire Notes Line
	5900 1950 7700 1950
Wire Notes Line
	5900 600  5900 1950
Text Notes 6250 1800 0    50   ~ 0
Active Low sets the translator to \npredefined home state:\n(45 Step Angle, DIR = H)\nSTEP inputs are ignored untill \nRESET goes high.
Connection ~ 6150 1000
Wire Wire Line
	6150 850  6150 1000
Wire Wire Line
	6150 1500 6150 1700
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EB2F983
P 6150 1700
F 0 "#GND?" H 6200 1650 45  0001 L BNN
F 1 "GND" H 6150 1530 45  0000 C CNN
F 2 "" H 6150 1600 60  0001 C CNN
F 3 "" H 6150 1600 60  0001 C CNN
	1    6150 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1100 6150 1000
$Comp
L dk_Pushbutton-Switches:GPTS203211B S?
U 1 1 5EB27173
P 6150 1300
F 0 "S?" V 6196 1256 50  0000 R CNN
F 1 "GPTS203211B" V 6105 1256 50  0000 R CNN
F 2 "digikey-footprints:PushButton_12x12mm_THT_GPTS203211B" H 6350 1500 50  0001 L CNN
F 3 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H 6350 1600 60  0001 L CNN
F 4 "CW181-ND" H 6350 1700 60  0001 L CNN "Digi-Key_PN"
F 5 "GPTS203211B" H 6350 1800 60  0001 L CNN "MPN"
F 6 "Switches" H 6350 1900 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H 6350 2000 60  0001 L CNN "Family"
F 8 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H 6350 2100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cw-industries/GPTS203211B/CW181-ND/3190590" H 6350 2200 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSHBUTTON SPST 1A 30V" H 6350 2300 60  0001 L CNN "Description"
F 11 "CW Industries" H 6350 2400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6350 2500 60  0001 L CNN "Status"
	1    6150 1300
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EB181CE
P 6150 850
F 0 "#SUPPLY?" H 6200 850 45  0001 L BNN
F 1 "VCC" H 6150 1020 45  0000 C CNN
F 2 "" H 6150 1031 60  0000 C CNN
F 3 "" H 6150 850 60  0001 C CNN
	1    6150 850 
	1    0    0    -1  
$EndComp
Text GLabel 2550 3950 2    47   Output ~ 0
OUT2B
Text GLabel 2550 3850 2    47   Output ~ 0
OUT2A
Wire Wire Line
	6150 1000 6550 1000
Text GLabel 6950 1000 2    47   Output ~ 0
RESET
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EB982DD
P 6750 1000
F 0 "R?" H 6750 1300 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6750 1216 45  0000 C CNN
F 2 "AXIAL-0.3" H 6750 1150 20  0001 C CNN
F 3 "" H 6750 1000 60  0001 C CNN
F 4 "RES-12183" H 6750 1121 60  0000 C CNN "Field4"
	1    6750 1000
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5ECC1D70
P 6200 3350
F 0 "#GND?" H 6250 3300 45  0001 L BNN
F 1 "GND" H 6200 3180 45  0000 C CNN
F 2 "" H 6200 3250 60  0001 C CNN
F 3 "" H 6200 3250 60  0001 C CNN
	1    6200 3350
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:POTENTIOMETER-PTH-9MM-1_20W-20% VR?
U 1 1 5EC83193
P 6200 2650
F 0 "VR?" H 6132 2555 45  0000 R CNN
F 1 "POTENTIOMETER-PTH-9MM-1_20W-20%" H 6132 2639 45  0000 R CNN
F 2 "POT-PTH-ALPS" V 6050 2650 20  0001 C CNN
F 3 "" H 6200 2650 60  0001 C CNN
F 4 "RES-09177" H 6132 2734 60  0000 R CNN "Field4"
	1    6200 2650
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Resistors:2.49KOHM-0603-1_10W-1% R?
U 1 1 5EC823D7
P 6200 3150
F 0 "R?" V 6105 3218 45  0000 L CNN
F 1 "2.49KOHM-0603-1_10W-1%" V 6189 3218 45  0000 L CNN
F 2 "0603" H 6200 3300 20  0001 C CNN
F 3 "" H 6200 3150 60  0001 C CNN
F 4 "RES-09568" V 6284 3218 60  0000 L CNN "Field4"
	1    6200 3150
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EC6CFA3
P 6200 2350
F 0 "#SUPPLY?" H 6250 2350 45  0001 L BNN
F 1 "VCC" H 6200 2520 45  0000 C CNN
F 2 "" H 6200 2531 60  0000 C CNN
F 3 "" H 6200 2350 60  0001 C CNN
	1    6200 2350
	1    0    0    -1  
$EndComp
Text Notes 6300 2450 0    47   ~ 0
At REF of 5V -> 833mA/phase\nAt REF of 2V -> 333mA/phase\n\nMinimum current = smootest steps\nMaximum current = highest torque
Text GLabel 6000 2600 1    47   Output ~ 0
REF
Wire Wire Line
	6000 2650 6000 2600
Wire Notes Line
	5900 2000 5900 3600
Wire Notes Line
	5900 3600 7700 3600
Wire Notes Line
	7700 3600 7700 2000
Wire Notes Line
	5900 2000 7700 2000
Wire Notes Line
	5900 5750 7700 5750
Wire Wire Line
	2150 3050 2150 3150
Wire Wire Line
	1950 3050 2050 3050
Wire Wire Line
	2050 3050 2050 3150
Wire Wire Line
	1950 3050 1950 3150
Connection ~ 1950 3050
Wire Notes Line
	4050 4750 5850 4750
Wire Notes Line
	5850 4750 5850 2800
Wire Notes Line
	5850 2800 4050 2800
Wire Notes Line
	4050 2800 4050 4750
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5F10EFA5
P 1900 2650
F 0 "#SUPPLY?" H 1950 2650 45  0001 L BNN
F 1 "VOUT" H 1900 2820 45  0000 C CNN
F 2 "" H 1900 2831 60  0000 C CNN
F 3 "" H 1900 2650 60  0001 C CNN
	1    1900 2650
	0    -1   -1   0   
$EndComp
Text Notes 650  1700 0    50   ~ 0
+- 750mA, 30V output rating\nMin 4.75V load voltage\n3.0 - 5.5 V logic supply\nCrossover protection\nMixed, fast, & slow decay modes\n
Text Notes 650  1150 0    50   ~ 0
Allegro A3967 Microstepping Driver with translator\n\nSimilar to what EasyDriver uses. Nifty little IC that\ntakes care of almost all the logic of it. 
Wire Notes Line
	5850 2750 5850 600 
Wire Notes Line
	5850 600  4050 600 
Wire Notes Line
	4050 600  4050 2750
Wire Notes Line
	4050 2750 5850 2750
Wire Notes Line
	500  6100 7750 6100
$EndSCHEMATC
