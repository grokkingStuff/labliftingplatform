EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 2 6
Title "Lab Lifting Platform"
Date ""
Rev ""
Comp "Heriot Watt University, Dubai"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SparkFun-Capacitors:47UF-POLAR-EIA3528-10V-10% C?
U 1 1 5EBC1FBC
P 4100 7550
AR Path="/5EBC1FBC" Ref="C?"  Part="1" 
AR Path="/5EB96250/5EBC1FBC" Ref="C3"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FBC" Ref="C?"  Part="1" 
F 0 "C3" V 4460 7500 45  0000 C CNN
F 1 "47UF-POLAR-EIA3528-10V-10%" V 4376 7500 45  0000 C CNN
F 2 "EIA3528" H 4100 7800 20  0001 C CNN
F 3 "" H 4100 7550 50  0001 C CNN
F 4 "CAP-08310" V 4281 7500 60  0000 C CNN "Field4"
	1    4100 7550
	0    -1   -1   0   
$EndComp
$Comp
L dk_PMIC-Motor-Drivers-Controllers:A3967SLBTR-T U?
U 1 1 5EBC1F95
P 5050 2150
AR Path="/5EBC1F95" Ref="U?"  Part="1" 
AR Path="/5EB96250/5EBC1F95" Ref="U2"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F95" Ref="U?"  Part="1" 
F 0 "U2" H 5250 1900 60  0000 C CNN
F 1 "A3967SLBTR-T" V 5550 1700 60  0000 C CNN
F 2 "digikey-footprints:SOIC-24_W7.50mm" H 5250 2350 60  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H 5250 2450 60  0001 L CNN
F 4 "620-1140-1-ND" H 5250 2550 60  0001 L CNN "Digi-Key_PN"
F 5 "A3967SLBTR-T" H 5250 2650 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5250 2750 60  0001 L CNN "Category"
F 7 "PMIC - Motor Drivers, Controllers" H 5250 2850 60  0001 L CNN "Family"
F 8 "https://www.allegromicro.com/~/media/Files/Datasheets/A3967-Datasheet.ashx" H 5250 2950 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/allegro-microsystems-llc/A3967SLBTR-T/620-1140-1-ND/1090383" H 5250 3050 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MTR DRV BIPOLAR 3-5.5V 24SOIC" H 5250 3150 60  0001 L CNN "Description"
F 11 "Allegro MicroSystems, LLC" H 5250 3250 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5250 3350 60  0001 L CNN "Status"
	1    5050 2150
	1    0    0    -1  
$EndComp
Text Notes 1350 6100 0    50   ~ 0
If V at PFD is greater than 0.6 VCC,\nslow decay mode is selected.\nAllows the motor is stop quickly.\n\nIf V at PFD is less than 0.21 VCC,\nfast decay mode is selected.\nResponds more quickly to changing \nstep inputs but stops slowly.\n\nElse, mixed decay mode is selected.\nAllows for microstepping to use the\nbest of both worlds. Mixed mode is\nwhat is selected here (0.4 VCC)
Text Notes 900  1400 0    50   ~ 0
Allegro A3967 Microstepping Driver \nwith in-built translator\n\nSimilar to what EasyDriver uses. Nifty \nlittle IC that takes care of almost all \nthe logic of controlling a motor. 
Text Notes 900  1950 0    50   ~ 0
+- 750mA, 30V output rating\nMin 4.75V load voltage\n3.0 - 5.5 V logic supply\nCrossover protection\nMixed, fast, & slow decay modes\n
$Comp
L SparkFun-PowerSymbols:VOUT #SUPPLY?
U 1 1 5EBC202D
P 4000 7550
AR Path="/5EBC202D" Ref="#SUPPLY?"  Part="1" 
AR Path="/5EB96250/5EBC202D" Ref="#SUPPLY0102"  Part="1" 
AR Path="/5EBB5EBC/5EBC202D" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY0102" H 4050 7550 45  0001 L BNN
F 1 "VOUT" H 4000 7720 45  0000 C CNN
F 2 "" H 4000 7731 60  0000 C CNN
F 3 "" H 4000 7550 60  0001 C CNN
	1    4000 7550
	0    -1   -1   0   
$EndComp
Text GLabel 3350 6000 1    47   Output ~ 0
REF
Text Notes 3650 5850 0    47   ~ 0
At REF of 5V -> 833mA/phase\nAt REF of 2V -> 333mA/phase\n\nMinimum current = smootest steps\nMaximum current = highest torque
$Comp
L SparkFun-Resistors:2.49KOHM-0603-1_10W-1% R?
U 1 1 5EBC201F
P 3550 6550
AR Path="/5EBC201F" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC201F" Ref="R5"  Part="1" 
AR Path="/5EBB5EBC/5EBC201F" Ref="R?"  Part="1" 
F 0 "R5" V 3455 6618 45  0000 L CNN
F 1 "2.49KOHM-0603-1_10W-1%" V 3539 6618 45  0000 L CNN
F 2 "0603" H 3550 6700 20  0001 C CNN
F 3 "" H 3550 6550 60  0001 C CNN
F 4 "RES-09568" V 3634 6618 60  0000 L CNN "Field4"
	1    3550 6550
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:POTENTIOMETER-PTH-9MM-1_20W-20% VR?
U 1 1 5EBC2018
P 3550 6050
AR Path="/5EBC2018" Ref="VR?"  Part="1" 
AR Path="/5EB96250/5EBC2018" Ref="VR1"  Part="1" 
AR Path="/5EBB5EBC/5EBC2018" Ref="VR?"  Part="1" 
F 0 "VR1" H 3482 5955 45  0000 R CNN
F 1 "POTENTIOMETER-PTH-9MM-1_20W-20%" H 3482 6039 45  0000 R CNN
F 2 "POT-PTH-ALPS" V 3400 6050 20  0001 C CNN
F 3 "" H 3550 6050 60  0001 C CNN
F 4 "RES-09177" H 3482 6134 60  0000 R CNN "Field4"
	1    3550 6050
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC2011
P 3550 6750
AR Path="/5EBC2011" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC2011" Ref="#GND0103"  Part="1" 
AR Path="/5EBB5EBC/5EBC2011" Ref="#GND?"  Part="1" 
F 0 "#GND0103" H 3600 6700 45  0001 L BNN
F 1 "GND" H 3550 6580 45  0000 C CNN
F 2 "" H 3550 6650 60  0001 C CNN
F 3 "" H 3550 6650 60  0001 C CNN
	1    3550 6750
	1    0    0    -1  
$EndComp
Text GLabel 4300 4400 2    47   Output ~ 0
RESET
Text GLabel 5550 2050 2    47   Output ~ 0
OUT2A
Text GLabel 5550 2150 2    47   Output ~ 0
OUT2B
Text Notes 3600 5200 0    50   ~ 0
Active Low sets the translator to \npredefined home state:\n(45 Step Angle, DIR = H)\nSTEP inputs are ignored untill \nRESET goes high.
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EBC1FE5
P 6150 6600
AR Path="/5EBC1FE5" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1FE5" Ref="R8"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FE5" Ref="R?"  Part="1" 
F 0 "R8" H 6150 6900 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6150 6816 45  0000 C CNN
F 2 "AXIAL-0.3" H 6150 6750 20  0001 C CNN
F 3 "" H 6150 6600 60  0001 C CNN
F 4 "RES-12183" H 6150 6721 60  0000 C CNN "Field4"
	1    6150 6600
	1    0    0    -1  
$EndComp
Text GLabel 6350 6600 2    47   Output ~ 0
SLEEP
Text Notes 5600 4500 0    50   ~ 0
When logic high, outputs are disabled.\nInputs (STEP, DIR, MS1, MS2) are still\naccessible.
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1FD6
P 5750 4150
AR Path="/5EBC1FD6" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1FD6" Ref="#GND0105"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FD6" Ref="#GND?"  Part="1" 
F 0 "#GND0105" H 5800 4100 45  0001 L BNN
F 1 "GND" H 5750 3980 45  0000 C CNN
F 2 "" H 5750 4050 60  0001 C CNN
F 3 "" H 5750 4050 60  0001 C CNN
	1    5750 4150
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1FD0
P 5750 5400
AR Path="/5EBC1FD0" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1FD0" Ref="#GND0106"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FD0" Ref="#GND?"  Part="1" 
F 0 "#GND0106" H 5800 5350 45  0001 L BNN
F 1 "GND" H 5750 5230 45  0000 C CNN
F 2 "" H 5750 5300 60  0001 C CNN
F 3 "" H 5750 5300 60  0001 C CNN
	1    5750 5400
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1FCA
P 5750 5000
AR Path="/5EBC1FCA" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1FCA" Ref="#GND0107"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FCA" Ref="#GND?"  Part="1" 
F 0 "#GND0107" H 5800 4950 45  0001 L BNN
F 1 "GND" H 5750 4830 45  0000 C CNN
F 2 "" H 5750 4900 60  0001 C CNN
F 3 "" H 5750 4900 60  0001 C CNN
	1    5750 5000
	0    1    1    0   
$EndComp
Text Notes 5600 5850 0    50   ~ 0
To minimize voltage drop from current \nsensing to ground, use a low value \nresistor, connect to ground with individual \npaths as close as possible. 
Text Notes 900  2400 0    50   ~ 0
Star ground system located close \nto driver is recommended. The analog\n ground and power ground are \ninternally bonded. (PINS 6,7,18,19)
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1FC2
P 4300 7550
AR Path="/5EBC1FC2" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1FC2" Ref="#GND0108"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FC2" Ref="#GND?"  Part="1" 
F 0 "#GND0108" H 4350 7500 45  0001 L BNN
F 1 "GND" H 4300 7380 45  0000 C CNN
F 2 "" H 4300 7450 60  0001 C CNN
F 3 "" H 4300 7450 60  0001 C CNN
	1    4300 7550
	0    -1   -1   0   
$EndComp
Text GLabel 4550 2750 0    48   Input ~ 0
PFD
Text GLabel 4550 2350 0    47   Input ~ 0
ENABLE
Text GLabel 4550 2050 0    47   Input ~ 0
DIR
Text GLabel 4550 1950 0    47   Input ~ 0
STEP
Text GLabel 4550 2150 0    47   Input ~ 0
MS1
Text GLabel 4550 1850 0    47   Input ~ 0
SENSE2
Text GLabel 4550 2450 0    47   Input ~ 0
SENSE1
Text GLabel 4550 2650 0    47   Input ~ 0
RC1
Text GLabel 4550 1650 0    47   Input ~ 0
RC2
Text GLabel 4550 2550 0    47   Input ~ 0
RESET
Text GLabel 4550 2250 0    47   Input ~ 0
MS2
Text GLabel 4550 1750 0    47   Input ~ 0
SLEEP
Text GLabel 5550 1950 2    47   Output ~ 0
OUT1B
Text GLabel 5550 1850 2    47   Output ~ 0
OUT1A
Text GLabel 4550 1550 0    47   Output ~ 0
REF
Text Notes 850  900  0    157  Italic 0
Motor Driver\n
Text GLabel 1300 4650 2    48   Input ~ 0
PFD
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5EBC1F76
P 1250 4850
AR Path="/5EBC1F76" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F76" Ref="R2"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F76" Ref="R?"  Part="1" 
F 0 "R2" V 1155 4918 45  0000 L CNN
F 1 "10KOHM-0603-1_10W-1%" V 1239 4918 45  0000 L CNN
F 2 "0603" H 1250 5000 20  0001 C CNN
F 3 "" H 1250 4850 60  0001 C CNN
F 4 "RES-00824" V 1334 4918 60  0000 L CNN "Field4"
	1    1250 4850
	0    1    -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1F6F
P 1250 5050
AR Path="/5EBC1F6F" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1F6F" Ref="#GND0110"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F6F" Ref="#GND?"  Part="1" 
F 0 "#GND0110" H 1300 5000 45  0001 L BNN
F 1 "GND" H 1250 4880 45  0000 C CNN
F 2 "" H 1250 4950 60  0001 C CNN
F 3 "" H 1250 4950 60  0001 C CNN
	1    1250 5050
	-1   0    0    -1  
$EndComp
Text GLabel 6900 4150 2    47   Output ~ 0
ENABLE
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EBC1F68
P 1650 6950
AR Path="/5EBC1F68" Ref="C?"  Part="1" 
AR Path="/5EB96250/5EBC1F68" Ref="C1"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F68" Ref="C?"  Part="1" 
F 0 "C1" V 1310 7000 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V 1394 7000 45  0000 C CNN
F 2 "0603" H 1650 7200 20  0001 C CNN
F 3 "" H 1650 6950 50  0001 C CNN
F 4 "CAP-09232" V 1489 7000 60  0000 C CNN "Field4"
	1    1650 6950
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EBC1F61
P 1700 6500
AR Path="/5EBC1F61" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F61" Ref="R3"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F61" Ref="R?"  Part="1" 
F 0 "R3" H 1700 6694 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H 1700 6610 45  0000 C CNN
F 2 "0603" H 1700 6650 20  0001 C CNN
F 3 "" H 1700 6500 60  0001 C CNN
	1    1700 6500
	1    0    0    -1  
$EndComp
Text GLabel 2300 6500 2    47   Output ~ 0
RC2
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EBC1F5A
P 6300 5400
AR Path="/5EBC1F5A" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F5A" Ref="R12"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F5A" Ref="R?"  Part="1" 
F 0 "R12" H 6300 5700 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H 6300 5616 45  0000 C CNN
F 2 "0805" H 6300 5550 20  0001 C CNN
F 3 "" H 6300 5400 60  0001 C CNN
F 4 "RES-08474" H 6300 5521 60  0000 C CNN "Field4"
	1    6300 5400
	1    0    0    -1  
$EndComp
Text GLabel 6900 5400 2    47   Output ~ 0
SENSE2
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1F52
P 1150 7900
AR Path="/5EBC1F52" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1F52" Ref="#GND0111"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F52" Ref="#GND?"  Part="1" 
F 0 "#GND0111" H 1200 7850 45  0001 L BNN
F 1 "GND" H 1150 7730 45  0000 C CNN
F 2 "" H 1150 7800 60  0001 C CNN
F 3 "" H 1150 7800 60  0001 C CNN
	1    1150 7900
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Capacitors:680PF-0603-50V-10% C?
U 1 1 5EBC1F4C
P 1650 7800
AR Path="/5EBC1F4C" Ref="C?"  Part="1" 
AR Path="/5EB96250/5EBC1F4C" Ref="C2"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F4C" Ref="C?"  Part="1" 
F 0 "C2" V 1310 7850 45  0000 C CNN
F 1 "680PF-0603-50V-10%" V 1394 7850 45  0000 C CNN
F 2 "0603" H 1650 8050 20  0001 C CNN
F 3 "" H 1650 7800 50  0001 C CNN
F 4 "CAP-09232" V 1489 7850 60  0000 C CNN "Field4"
	1    1650 7800
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:20KOHM-0603-1_10W-1% R?
U 1 1 5EBC1F45
P 1700 7350
AR Path="/5EBC1F45" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F45" Ref="R4"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F45" Ref="R?"  Part="1" 
F 0 "R4" H 1700 7544 45  0000 C CNN
F 1 "20KOHM-0603-1_10W-1%" H 1700 7460 45  0000 C CNN
F 2 "0603" H 1700 7500 20  0001 C CNN
F 3 "" H 1700 7350 60  0001 C CNN
	1    1700 7350
	1    0    0    -1  
$EndComp
Text GLabel 2300 7350 2    47   Output ~ 0
RC1
$Comp
L SparkFun-Resistors:0.75OHM-0805-1_4W-1% R?
U 1 1 5EBC1F3E
P 6300 5000
AR Path="/5EBC1F3E" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F3E" Ref="R11"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F3E" Ref="R?"  Part="1" 
F 0 "R11" H 6300 5300 45  0000 C CNN
F 1 "0.75OHM-0805-1_4W-1%" H 6300 5216 45  0000 C CNN
F 2 "0805" H 6300 5150 20  0001 C CNN
F 3 "" H 6300 5000 60  0001 C CNN
F 4 "RES-08474" H 6300 5121 60  0000 C CNN "Field4"
	1    6300 5000
	1    0    0    -1  
$EndComp
Text GLabel 6900 5000 2    47   Output ~ 0
SENSE1
Text GLabel 6350 7400 2    47   Output ~ 0
MS2
Text GLabel 6350 7000 2    47   Output ~ 0
MS1
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EBC1F34
P 6150 7400
AR Path="/5EBC1F34" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F34" Ref="R10"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F34" Ref="R?"  Part="1" 
F 0 "R10" H 6150 7700 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6150 7616 45  0000 C CNN
F 2 "AXIAL-0.3" H 6150 7550 20  0001 C CNN
F 3 "" H 6150 7400 60  0001 C CNN
F 4 "RES-12183" H 6150 7521 60  0000 C CNN "Field4"
	1    6150 7400
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EBC1F2D
P 6150 7000
AR Path="/5EBC1F2D" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F2D" Ref="R9"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F2D" Ref="R?"  Part="1" 
F 0 "R9" H 6150 7300 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 6150 7216 45  0000 C CNN
F 2 "AXIAL-0.3" H 6150 7150 20  0001 C CNN
F 3 "" H 6150 7000 60  0001 C CNN
F 4 "RES-12183" H 6150 7121 60  0000 C CNN "Field4"
	1    6150 7000
	1    0    0    -1  
$EndComp
Wire Notes Line
	1000 6150 2800 6150
Wire Notes Line
	1000 4000 1000 6150
Wire Notes Line
	2800 4000 1000 4000
Wire Notes Line
	2800 6150 2800 4000
Wire Notes Line
	1000 6200 1000 8150
Wire Notes Line
	2800 6200 1000 6200
Wire Notes Line
	2800 8150 2800 6200
Wire Notes Line
	1000 8150 2800 8150
Wire Wire Line
	4950 1250 4950 1350
Wire Wire Line
	5050 1250 5050 1350
Wire Wire Line
	4950 1250 5050 1250
Wire Wire Line
	5150 1250 5150 1350
Wire Notes Line
	5500 4550 7300 4550
Wire Notes Line
	3250 5400 5050 5400
Wire Notes Line
	5050 7000 5050 5400
Wire Notes Line
	3250 7000 5050 7000
Wire Notes Line
	3250 5400 3250 7000
Wire Wire Line
	3350 6050 3350 6000
$Comp
L SparkFun-Resistors:10KOHM-HORIZ-1_4W-1% R?
U 1 1 5EBC1F13
P 4100 4400
AR Path="/5EBC1F13" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F13" Ref="R6"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F13" Ref="R?"  Part="1" 
F 0 "R6" H 4100 4700 45  0000 C CNN
F 1 "10KOHM-HORIZ-1_4W-1%" H 4100 4616 45  0000 C CNN
F 2 "AXIAL-0.3" H 4100 4550 20  0001 C CNN
F 3 "" H 4100 4400 60  0001 C CNN
F 4 "RES-12183" H 4100 4521 60  0000 C CNN "Field4"
	1    4100 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4400 3900 4400
$Comp
L dk_Pushbutton-Switches:GPTS203211B S?
U 1 1 5EBC1F03
P 3500 4700
AR Path="/5EBC1F03" Ref="S?"  Part="1" 
AR Path="/5EB96250/5EBC1F03" Ref="S1"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F03" Ref="S?"  Part="1" 
F 0 "S1" V 3546 4656 50  0000 R CNN
F 1 "GPTS203211B" V 3455 4656 50  0000 R CNN
F 2 "digikey-footprints:PushButton_12x12mm_THT_GPTS203211B" H 3700 4900 50  0001 L CNN
F 3 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H 3700 5000 60  0001 L CNN
F 4 "CW181-ND" H 3700 5100 60  0001 L CNN "Digi-Key_PN"
F 5 "GPTS203211B" H 3700 5200 60  0001 L CNN "MPN"
F 6 "Switches" H 3700 5300 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H 3700 5400 60  0001 L CNN "Family"
F 8 "http://switches-connectors-custom.cwind.com/Asset/GPTS203211BR2.pdf" H 3700 5500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cw-industries/GPTS203211B/CW181-ND/3190590" H 3700 5600 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSHBUTTON SPST 1A 30V" H 3700 5700 60  0001 L CNN "Description"
F 11 "CW Industries" H 3700 5800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3700 5900 60  0001 L CNN "Status"
	1    3500 4700
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1EF3
P 3500 5100
AR Path="/5EBC1EF3" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1EF3" Ref="#GND0112"  Part="1" 
AR Path="/5EBB5EBC/5EBC1EF3" Ref="#GND?"  Part="1" 
F 0 "#GND0112" H 3550 5050 45  0001 L BNN
F 1 "GND" H 3500 4930 45  0000 C CNN
F 2 "" H 3500 5000 60  0001 C CNN
F 3 "" H 3500 5000 60  0001 C CNN
	1    3500 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4900 3500 5100
Wire Wire Line
	3500 4250 3500 4400
Wire Notes Line
	3250 4000 3250 5350
Wire Notes Line
	3250 5350 5050 5350
Wire Notes Line
	5050 5350 5050 4000
Wire Notes Line
	3250 4000 5050 4000
Wire Wire Line
	5950 7400 5650 7400
Wire Wire Line
	5950 7000 5650 7000
Wire Wire Line
	5950 6600 5650 6600
Wire Notes Line
	5500 6000 7300 6000
Wire Notes Line
	5500 7500 5500 6000
Wire Notes Line
	5500 4000 7300 4000
Wire Notes Line
	7300 4550 7300 4000
Wire Notes Line
	5500 4000 5500 4550
Wire Notes Line
	5500 5900 7300 5900
Wire Notes Line
	5500 4650 7300 4650
Wire Notes Line
	7300 5900 7300 4650
Wire Notes Line
	5500 4650 5500 5900
Text GLabel 6350 6600 2    47   Output ~ 0
SLEEP
Wire Wire Line
	5650 6300 5650 6600
Wire Wire Line
	5750 4150 6900 4150
Text Notes 5600 4500 0    50   ~ 0
When logic high, outputs are disabled.\nInputs (STEP, DIR, MS1, MS2) are still\naccessible.
Text Notes 5600 5850 0    50   ~ 0
To minimize voltage drop from current \nsensing to ground, use a low value \nresistor, connect to ground with individual \npaths as close as possible. 
Wire Wire Line
	5150 2950 5150 3050
Wire Wire Line
	5150 3050 5050 3050
Wire Wire Line
	5050 3050 4950 3050
Connection ~ 5050 3050
Wire Wire Line
	5050 2950 5050 3050
Wire Wire Line
	4950 2950 4950 3050
Connection ~ 5150 3050
Wire Wire Line
	5250 3050 5150 3050
Wire Wire Line
	5250 2950 5250 3050
Wire Wire Line
	1300 4650 1250 4650
Text GLabel 6900 4150 2    47   Output ~ 0
ENABLE
Wire Wire Line
	6500 5400 6900 5400
Wire Wire Line
	2300 6500 2200 6500
Wire Wire Line
	5750 5400 6100 5400
Wire Wire Line
	2200 6950 1850 6950
Wire Wire Line
	2200 6500 2200 6950
Connection ~ 2200 6500
Wire Wire Line
	1900 6500 2200 6500
Wire Wire Line
	1150 6500 1150 6950
Connection ~ 1150 6950
Wire Wire Line
	1150 6950 1550 6950
Wire Wire Line
	1500 6500 1150 6500
Wire Wire Line
	6500 5000 6900 5000
Wire Wire Line
	2300 7350 2200 7350
Wire Wire Line
	5750 5000 6100 5000
Wire Wire Line
	2200 7800 1850 7800
Wire Wire Line
	2200 7350 2200 7800
Connection ~ 2200 7350
Wire Wire Line
	1900 7350 2200 7350
Wire Wire Line
	1150 7800 1150 7900
Connection ~ 1150 7800
Wire Wire Line
	1150 7800 1550 7800
Wire Wire Line
	1150 6950 1150 7350
Wire Wire Line
	1150 7350 1150 7800
Connection ~ 1150 7350
Wire Wire Line
	1500 7350 1150 7350
Connection ~ 5650 6600
Wire Wire Line
	5650 6600 5650 7000
Connection ~ 5650 7000
Wire Wire Line
	5650 7000 5650 7400
Text GLabel 6350 7400 2    47   Output ~ 0
MS2
Text GLabel 6350 7000 2    47   Output ~ 0
MS1
Wire Notes Line
	3250 7800 5050 7800
Wire Notes Line
	3250 7800 3250 7050
Wire Notes Line
	3250 7050 5050 7050
Wire Notes Line
	5050 7050 5050 7800
Connection ~ 1250 4650
$Comp
L SparkFun-Resistors:6.8KOHM-0603-1_10W-1% R?
U 1 1 5EBC1F7D
P 1250 4450
AR Path="/5EBC1F7D" Ref="R?"  Part="1" 
AR Path="/5EB96250/5EBC1F7D" Ref="R1"  Part="1" 
AR Path="/5EBB5EBC/5EBC1F7D" Ref="R?"  Part="1" 
F 0 "R1" V 1345 4382 45  0000 R CNN
F 1 "6.8KOHM-0603-1_10W-1%" V 1261 4382 45  0000 R CNN
F 2 "0603" H 1250 4600 20  0001 C CNN
F 3 "" H 1250 4450 60  0001 C CNN
F 4 "RES-08597" V 1166 4382 60  0000 R CNN "Field4"
	1    1250 4450
	0    -1   1    0   
$EndComp
Connection ~ 3500 4400
Wire Wire Line
	3500 4500 3500 4400
Connection ~ 4950 3050
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EBC1FA5
P 4950 3050
AR Path="/5EBC1FA5" Ref="#GND?"  Part="1" 
AR Path="/5EB96250/5EBC1FA5" Ref="#GND0109"  Part="1" 
AR Path="/5EBB5EBC/5EBC1FA5" Ref="#GND?"  Part="1" 
F 0 "#GND0109" H 5000 3000 45  0001 L BNN
F 1 "GND" H 4950 2880 45  0000 C CNN
F 2 "" H 4950 2950 60  0001 C CNN
F 3 "" H 4950 2950 60  0001 C CNN
	1    4950 3050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:5V #SUPPLY0101
U 1 1 5EA406A8
P 3550 5750
F 0 "#SUPPLY0101" H 3600 5750 45  0001 L BNN
F 1 "5V" H 3550 5920 45  0000 C CNN
F 2 "" H 3550 5931 60  0000 C CNN
F 3 "" H 3550 5750 60  0001 C CNN
	1    3550 5750
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:VIN #SUPPLY0103
U 1 1 5EA4430E
P 4950 1250
F 0 "#SUPPLY0103" H 5000 1250 45  0001 L BNN
F 1 "VIN" H 4950 1420 45  0000 C CNN
F 2 "" H 4950 1431 60  0000 C CNN
F 3 "" H 4950 1250 60  0001 C CNN
	1    4950 1250
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Connectors:CONN_04SCREW_LOCK J?
U 1 1 5EA47CD0
P 6050 7750
F 0 "J?" H 5822 7858 45  0000 R CNN
F 1 "CONN_04SCREW_LOCK" H 5822 7942 45  0000 R CNN
F 2 "SCREWTERMINAL-3.5MM-4_LOCK" H 6050 8250 20  0001 C CNN
F 3 "" H 6050 7750 50  0001 C CNN
F 4 "" H 5822 7984 60  0000 R CNN "Field4"
	1    6050 7750
	-1   0    0    1   
$EndComp
Text GLabel 5950 7950 0    47   Input ~ 0
OUT2A
Text GLabel 5950 8050 0    47   Input ~ 0
OUT2B
Text GLabel 5950 7850 0    47   Input ~ 0
OUT1B
Text GLabel 5950 7750 0    47   Input ~ 0
OUT1A
Wire Notes Line
	5500 7500 7300 7500
Wire Notes Line
	7300 6000 7300 7500
Wire Notes Line
	7300 7600 5500 7600
Text Notes 5700 8250 0    47   ~ 0
Screw Terminal 
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY0104
U 1 1 5EA8FA55
P 5150 1250
F 0 "#SUPPLY0104" H 5200 1250 45  0001 L BNN
F 1 "3.3V" H 5150 1420 45  0000 C CNN
F 2 "" H 5150 1431 60  0000 C CNN
F 3 "" H 5150 1250 60  0001 C CNN
	1    5150 1250
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY0105
U 1 1 5EA90970
P 1250 4250
F 0 "#SUPPLY0105" H 1300 4250 45  0001 L BNN
F 1 "3.3V" H 1250 4420 45  0000 C CNN
F 2 "" H 1250 4431 60  0000 C CNN
F 3 "" H 1250 4250 60  0001 C CNN
	1    1250 4250
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY0106
U 1 1 5EA917AA
P 3500 4250
F 0 "#SUPPLY0106" H 3550 4250 45  0001 L BNN
F 1 "3.3V" H 3500 4420 45  0000 C CNN
F 2 "" H 3500 4431 60  0000 C CNN
F 3 "" H 3500 4250 60  0001 C CNN
	1    3500 4250
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY0107
U 1 1 5EA925CE
P 5650 6300
F 0 "#SUPPLY0107" H 5700 6300 45  0001 L BNN
F 1 "3.3V" H 5650 6470 45  0000 C CNN
F 2 "" H 5650 6481 60  0000 C CNN
F 3 "" H 5650 6300 60  0001 C CNN
	1    5650 6300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
