EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	9800 5400 9950 5400
Connection ~ 9950 5400
Wire Wire Line
	9800 5800 9950 5800
Connection ~ 9950 5800
Wire Wire Line
	10100 5400 9950 5400
Wire Wire Line
	10100 5800 9950 5800
Text GLabel 1400 2800 0    50   Input ~ 0
DSDAM
Text GLabel 1400 2600 0    50   Input ~ 0
DSCLP
Text GLabel 2400 2600 2    50   Output ~ 0
SCL
Text GLabel 2400 2800 2    50   Output ~ 0
SDA
Wire Wire Line
	10100 4050 9950 4050
Wire Wire Line
	10100 3650 9950 3650
Connection ~ 9950 4050
Wire Wire Line
	9800 4050 9950 4050
Connection ~ 9950 3650
Wire Wire Line
	2400 2700 2700 2700
Wire Wire Line
	2400 2500 2700 2500
Wire Wire Line
	1400 2900 900  2900
Wire Wire Line
	2400 2900 2500 2900
Wire Wire Line
	900  3050 2500 3050
Wire Wire Line
	2500 2900 2500 3050
Wire Wire Line
	2500 3050 2500 3500
Wire Wire Line
	2500 3500 1800 3500
Connection ~ 2500 3050
Wire Wire Line
	2500 3500 2500 4000
Wire Wire Line
	2500 4000 1800 4000
Connection ~ 2500 3500
Wire Wire Line
	900  4000 1500 4000
Connection ~ 2700 2700
Wire Wire Line
	900  3500 900  4000
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC473B0
P 900 4750
AR Path="/5EC473B0" Ref="#GND?"  Part="1" 
AR Path="/5EC40A6A/5EC473B0" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 950 4700 45  0001 L BNN
F 1 "GND" H 900 4580 45  0000 C CNN
F 2 "" H 900 4650 60  0001 C CNN
F 3 "" H 900 4650 60  0001 C CNN
	1    900  4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  2900 900  3050
Wire Wire Line
	2700 2500 2700 2700
Wire Wire Line
	900  3500 1500 3500
Wire Wire Line
	1900 4450 1450 4450
Wire Wire Line
	2700 4450 2300 4450
Wire Wire Line
	2700 2700 2700 4450
Wire Wire Line
	1150 4450 900  4450
Wire Wire Line
	900  4450 900  4750
Wire Wire Line
	900  4000 900  4450
Connection ~ 900  4000
Connection ~ 900  4450
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC473C1
P 9950 4450
AR Path="/5EC473C1" Ref="#GND?"  Part="1" 
AR Path="/5EC40A6A/5EC473C1" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 10000 4400 45  0001 L BNN
F 1 "GND" H 9950 4280 45  0000 C CNN
F 2 "" H 9950 4350 60  0001 C CNN
F 3 "" H 9950 4350 60  0001 C CNN
	1    9950 4450
	1    0    0    -1  
$EndComp
Text GLabel 9800 5800 0    50   Input ~ 0
DSCL0_N
Text GLabel 9800 5400 0    50   Input ~ 0
DSCL0_P
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC473C9
P 9950 6200
AR Path="/5EC473C9" Ref="#GND?"  Part="1" 
AR Path="/5EC40A6A/5EC473C9" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 10000 6150 45  0001 L BNN
F 1 "GND" H 9950 6030 45  0000 C CNN
F 2 "" H 9950 6100 60  0001 C CNN
F 3 "" H 9950 6100 60  0001 C CNN
	1    9950 6200
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5EC473CF
P 9950 5000
AR Path="/5EC473CF" Ref="#SUPPLY?"  Part="1" 
AR Path="/5EC40A6A/5EC473CF" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 10000 5000 45  0001 L BNN
F 1 "3.3V" H 9950 5170 45  0000 C CNN
F 2 "" H 9950 5181 60  0000 C CNN
F 3 "" H 9950 5000 60  0001 C CNN
	1    9950 5000
	1    0    0    -1  
$EndComp
Text GLabel 10100 5400 2    50   Output ~ 0
DSCLP
Text GLabel 10100 5800 2    50   Output ~ 0
DSCLM
Text GLabel 1400 2700 0    50   Input ~ 0
DSDAP
$Comp
L SparkFun-Capacitors:0.1UF-0603-100V-10% C?
U 1 1 5EC473D9
P 1700 4000
AR Path="/5EC473D9" Ref="C?"  Part="1" 
AR Path="/5EC40A6A/5EC473D9" Ref="C?"  Part="1" 
F 0 "C?" V 2040 4050 45  0000 C CNN
F 1 "0.1UF-0603-100V-10%" V 1956 4050 45  0000 C CNN
F 2 "0603" H 1700 4250 20  0001 C CNN
F 3 "" H 1700 4000 50  0001 C CNN
F 4 "CAP-08390" V 1861 4050 60  0000 C CNN "Field4"
	1    1700 4000
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun-Capacitors:100UF-POLAR-10X10.5-63V-20% C?
U 1 1 5EC473E0
P 1700 3500
AR Path="/5EC473E0" Ref="C?"  Part="1" 
AR Path="/5EC40A6A/5EC473E0" Ref="C?"  Part="1" 
F 0 "C?" V 1340 3450 45  0000 C CNN
F 1 "100UF-POLAR-10X10.5-63V-20%" V 1424 3450 45  0000 C CNN
F 2 "NIC_10X10.5_CAP" H 1700 3750 20  0001 C CNN
F 3 "" H 1700 3500 50  0001 C CNN
F 4 "CAP-08362" V 1519 3450 60  0000 C CNN "Field4"
	1    1700 3500
	0    1    1    0   
$EndComp
Text GLabel 10100 3650 2    50   Output ~ 0
DSDAP
Text GLabel 10100 4050 2    50   Output ~ 0
DSDAM
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC473E9
P 9950 4250
AR Path="/5EC473E9" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC473E9" Ref="R?"  Part="1" 
F 0 "R?" V 9855 4318 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 4318 45  0000 L CNN
F 2 "0603" H 9950 4400 20  0001 C CNN
F 3 "" H 9950 4250 60  0001 C CNN
F 4 "RES-07857" V 10034 4318 60  0000 L CNN "Field4"
	1    9950 4250
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC473F0
P 9950 3850
AR Path="/5EC473F0" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC473F0" Ref="R?"  Part="1" 
F 0 "R?" V 9855 3918 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 3918 45  0000 L CNN
F 2 "0603" H 9950 4000 20  0001 C CNN
F 3 "" H 9950 3850 60  0001 C CNN
F 4 "RES-07857" V 10034 3918 60  0000 L CNN "Field4"
	1    9950 3850
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC473F7
P 9950 3450
AR Path="/5EC473F7" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC473F7" Ref="R?"  Part="1" 
F 0 "R?" V 9855 3518 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 3518 45  0000 L CNN
F 2 "0603" H 9950 3600 20  0001 C CNN
F 3 "" H 9950 3450 60  0001 C CNN
F 4 "RES-07857" V 10034 3518 60  0000 L CNN "Field4"
	1    9950 3450
	0    1    1    0   
$EndComp
$Comp
L SparkFun-PowerSymbols:3.3V #SUPPLY?
U 1 1 5EC473FD
P 9950 3250
AR Path="/5EC473FD" Ref="#SUPPLY?"  Part="1" 
AR Path="/5EC40A6A/5EC473FD" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 10000 3250 45  0001 L BNN
F 1 "3.3V" H 9950 3420 45  0000 C CNN
F 2 "" H 9950 3431 60  0000 C CNN
F 3 "" H 9950 3250 60  0001 C CNN
	1    9950 3250
	1    0    0    -1  
$EndComp
Text GLabel 9800 3650 0    50   Input ~ 0
DSDA0_P
Text GLabel 9800 4050 0    50   Input ~ 0
DSDA0_N
$Comp
L SparkFun-PowerSymbols:VCC #SUPPLY?
U 1 1 5EC47405
P 900 2300
AR Path="/5EC47405" Ref="#SUPPLY?"  Part="1" 
AR Path="/5EC40A6A/5EC47405" Ref="#SUPPLY?"  Part="1" 
F 0 "#SUPPLY?" H 950 2300 45  0001 L BNN
F 1 "VCC" H 900 2470 45  0000 C CNN
F 2 "" H 900 2481 60  0000 C CNN
F 3 "" H 900 2300 60  0001 C CNN
	1    900  2300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND?
U 1 1 5EC4740B
P 900 4750
AR Path="/5EC4740B" Ref="#GND?"  Part="1" 
AR Path="/5EC40A6A/5EC4740B" Ref="#GND?"  Part="1" 
F 0 "#GND?" H 950 4700 45  0001 L BNN
F 1 "GND" H 900 4580 45  0000 C CNN
F 2 "" H 900 4650 60  0001 C CNN
F 3 "" H 900 4650 60  0001 C CNN
	1    900  4750
	1    0    0    -1  
$EndComp
Text Notes 800  1050 0    157  Italic 0
Differential I2C
$Comp
L SparkFun-LED:LED-BLUE0603 D?
U 1 1 5EC47413
P 1350 4450
AR Path="/5EC47413" Ref="D?"  Part="1" 
AR Path="/5EC40A6A/5EC47413" Ref="D?"  Part="1" 
F 0 "D?" V 955 4400 45  0000 C CNN
F 1 "LED-BLUE0603" V 1039 4400 45  0000 C CNN
F 2 "LED-0603" V 1650 4450 20  0001 C CNN
F 3 "" H 1350 4450 50  0001 C CNN
F 4 "DIO-08575" V 1134 4400 60  0000 C CNN "Field4"
	1    1350 4450
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:10KOHM-0603-1_10W-1% R?
U 1 1 5EC4741A
P 2100 4450
AR Path="/5EC4741A" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC4741A" Ref="R?"  Part="1" 
F 0 "R?" H 2100 4750 45  0000 C CNN
F 1 "10KOHM-0603-1_10W-1%" H 2100 4666 45  0000 C CNN
F 2 "0603" H 2100 4600 20  0001 C CNN
F 3 "" H 2100 4450 60  0001 C CNN
F 4 "RES-00824" H 2100 4571 60  0000 C CNN "Field4"
	1    2100 4450
	1    0    0    -1  
$EndComp
Text GLabel 8450 4150 2    50   Output ~ 0
DSDA0_N
Text GLabel 8450 4050 2    50   Output ~ 0
DSDA0_P
Text GLabel 8450 3950 2    50   Output ~ 0
DSCL0_N
$Comp
L SparkFun-Connectors:CONN_04SCREW J?
U 1 1 5EC47424
P 8350 4150
AR Path="/5EC47424" Ref="J?"  Part="1" 
AR Path="/5EC40A6A/5EC47424" Ref="J?"  Part="1" 
F 0 "J?" H 8308 4760 45  0000 C CNN
F 1 "CONN_04SCREW" H 8308 4676 45  0000 C CNN
F 2 "SCREWTERMINAL-3.5MM-4" H 8350 4650 20  0001 C CNN
F 3 "" H 8350 4150 50  0001 C CNN
F 4 "2xCONN-08399" H 8308 4581 60  0000 C CNN "Field4"
	1    8350 4150
	1    0    0    -1  
$EndComp
Connection ~ 900  4750
Wire Notes Line
	9300 3000 11100 3000
Wire Notes Line
	9300 4700 11100 4700
Wire Notes Line
	9300 3000 9300 4700
Wire Notes Line
	11100 3000 11100 4700
Wire Notes Line
	9300 4750 11100 4750
Wire Notes Line
	11100 6450 9300 6450
Wire Notes Line
	9300 6450 9300 4750
Text GLabel 1400 2500 0    50   Input ~ 0
DSCLM
$Comp
L PCA9615:PCA9615 U?
U 1 1 5EC47433
P 1900 2700
AR Path="/5EC47433" Ref="U?"  Part="1" 
AR Path="/5EC40A6A/5EC47433" Ref="U?"  Part="1" 
F 0 "U?" H 1900 2263 60  0000 C CNN
F 1 "PCA9615" H 1900 2369 60  0000 C CNN
F 2 "" H 1900 2700 60  0001 C CNN
F 3 "" H 1900 2700 60  0001 C CNN
	1    1900 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9800 3650 9950 3650
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC4743B
P 9950 5600
AR Path="/5EC4743B" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC4743B" Ref="R?"  Part="1" 
F 0 "R?" V 9855 5668 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 5668 45  0000 L CNN
F 2 "0603" H 9950 5750 20  0001 C CNN
F 3 "" H 9950 5600 60  0001 C CNN
F 4 "RES-07857" V 10034 5668 60  0000 L CNN "Field4"
	1    9950 5600
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC47442
P 9950 5200
AR Path="/5EC47442" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC47442" Ref="R?"  Part="1" 
F 0 "R?" V 9855 5268 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 5268 45  0000 L CNN
F 2 "0603" H 9950 5350 20  0001 C CNN
F 3 "" H 9950 5200 60  0001 C CNN
F 4 "RES-07857" V 10034 5268 60  0000 L CNN "Field4"
	1    9950 5200
	0    1    1    0   
$EndComp
$Comp
L SparkFun-Resistors:4.7KOHM-0603-1_10W-1% R?
U 1 1 5EC47449
P 9950 6000
AR Path="/5EC47449" Ref="R?"  Part="1" 
AR Path="/5EC40A6A/5EC47449" Ref="R?"  Part="1" 
F 0 "R?" V 9855 6068 45  0000 L CNN
F 1 "4.7KOHM-0603-1_10W-1%" V 9939 6068 45  0000 L CNN
F 2 "0603" H 9950 6150 20  0001 C CNN
F 3 "" H 9950 6000 60  0001 C CNN
F 4 "RES-07857" V 10034 6068 60  0000 L CNN "Field4"
	1    9950 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	900  2300 900  2900
Connection ~ 900  2900
Text Notes 850  1800 0    50   ~ 0
Inter-Integrated Circuit (I2C)\n\nSDA: Serial Data, used to send and receive data\nSCL: Serial Clock, used to carry clock signal\n\nSynchronous protocol with only two wires. \nUsed quite a bit to interface with sensors.\n\n
Text GLabel 8450 3850 2    50   Output ~ 0
DSCL0_P
Wire Notes Line
	11100 4750 11100 6450
Text Notes 3050 1750 0    50   ~ 0
Why bother with differential I2C?\n\nUnfortunately, two conductive wires that extend for some \ndistance acts as a "bus capacitor" that  degrades signal \nquality. Using stronger pullup resistors can charge that \nparasitic capacitor quicker but also means the higher current \nthat flows through your fragile sensors, which places a hard \nlimit on such finesses. Also, low-to-high transitions take time\nwhile high-to-low transitions are instantaneous because of\nthe parasitic capacitance.\n\nUsing a differential pair helps reduce \nthe parastic losses and is more able to handle EMI.
$EndSCHEMATC
